package controller;

import api.ISistemaRecomendacionPeliculas;
import model.data_structures.ILista;
import model.logic.ManejadorPeliculas;
import model.vo.VOGeneroPelicula;
import model.vo.VOGeneroUsuario;
import model.vo.VOOperacion;
import model.vo.VOPelicula;
import model.vo.VOPeliculaPelicula;
import model.vo.VOPeliculaUsuario;
import model.vo.VORating;
import model.vo.VOTag;
import model.vo.VOUsuario;
import model.vo.VOUsuarioGenero;


public class Controller 
{

	private static ISistemaRecomendacionPeliculas sR = new ManejadorPeliculas<>();

	public static boolean cargarPeliculasSR(String rutaPeliculas)
	{
		return sR.cargarPeliculasSR(rutaPeliculas)  ;
	}
	
	public static boolean cargarRatingsSR(String rutaRatings)
	{
		return sR.cargarRatingsSR(rutaRatings);
	}
	
	public static boolean cargarTagsSR(String rutaTags)
	{
		return sR.cargarTagsSR(rutaTags);
	}
	public static int sizeMoviesSR() 
	{
		return sR.sizeMoviesSR();
	}
	
	public static int sizeUsersSR() 
	{
		return sR.sizeUsersSR();
	}
	
	public static int sizeTagsSR()
	{
		return sR.sizeTagsSR();
	}
	
	public static ILista<VOGeneroPelicula> peliculasPopularesSR (int n) 
	{
		return sR.peliculasPopularesSR(n);
	}
	
	public static ILista<VOPelicula> catalogoPeliculasOrdenadoSR() 
	{
		return sR.catalogoPeliculasOrdenadoSR();
	}
	
	public static ILista<VOGeneroPelicula> recomendarGeneroSR()
{
		return sR.recomendarGeneroSR();
	}
	
	public static ILista<VOGeneroPelicula> opinionRatingsGeneroSR() 
	{
		return sR.opinionRatingsGeneroSR();
	}
	
	public static ILista<VOPeliculaPelicula> recomendarPeliculasSR(String rutaRecomendacion, int n) 
	{
		return sR.recomendarPeliculasSR(rutaRecomendacion, n);
	}
	
	public static ILista<VORating> ratingsPeliculaSR (long idPelicula) 
	{
		return sR.ratingsPeliculaSR(idPelicula);
	}
	
	public static ILista<VOGeneroUsuario> usuariosActivosSR(int n) 
	{
		return sR.usuariosActivosSR(n);
	}
	
	public static ILista<VOUsuario> catalogoUsuariosOrdenadoSR() 
	{
		return sR.catalogoUsuariosOrdenadoSR();
	}
	
	public static ILista<VOGeneroPelicula> recomendarTagsGeneroSR(int n)
	{
		return sR.recomendarTagsGeneroSR(n);
	}
	
	public static ILista<VOUsuarioGenero> opinionTagsGeneroSR()
	{
		return sR.opinionTagsGeneroSR();
	}
	
	public static ILista<VOPeliculaUsuario> recomendarUsuariosSR(String rutaRecomendacion, int n) 
	{
		return sR.recomendarUsuariosSR(rutaRecomendacion, n);
	}
	
	public static ILista<VOTag> tagsPeliculaSR(int idPelicula)
	{
		return sR.tagsPeliculaSR(idPelicula);
	}
	
	public static void agregarOperacionSR(String nomOperacion, long tinicio, long tfin) 
	{
		sR.agregarOperacionSR(nomOperacion, tinicio, tfin);
	}
	
	public static ILista<VOOperacion> darHistoralOperacionesSR() 
	{
		return sR.darHistoralOperacionesSR();
	}
	
	public static void limpiarHistorialOperacionesSR() 
	{
		sR.limpiarHistorialOperacionesSR();
	}
	
	public static ILista<VOOperacion> darUltimasOperaciones(int n) 
	{
		return sR.darUltimasOperaciones(n);
	}
	
	public static void borrarUltimasOperaciones(int n)
	{
		sR.borrarUltimasOperaciones(n);
	}
	
	public static long agregarPelicula(String titulo, int agno, String[] generos) 
	{
		return sR.agregarPelicula(titulo, agno, generos);
	}
	
	public static void agregarRating(int idUsuario, int idPelicula, double rating) 
	{
		sR.agregarRating(idUsuario, idPelicula, rating);
	}
	
}
