package Test;


import junit.framework.TestCase;
import model.data_structures.ListaDobleEncadenada;

public class ListaDobleEncadenadaTest extends TestCase{

	//Atributos
		//Lista doble encadenada sobre la que se realizaran las pruebas
		private ListaDobleEncadenada    lista;
		
		/**
		 * Declaraci�n del escenario con los datos sobre los que se trabajar�.
		 */
		public void setupEscenario1()
		{
			lista = new ListaDobleEncadenada<>();
			lista.agregarElementoFinal("1");
			lista.agregarElementoFinal("2");
			lista.agregarElementoFinal("3");
			lista.agregarElementoFinal("4");
			lista.agregarElementoFinal("5");
			lista.agregarElementoFinal("6");
			lista.agregarElementoFinal("6");
			lista.agregarElementoFinal("7");
			
		}
		

		/**
		 * M�todo que realiza el test correspondiente al m�todo darNumeroElementos.
		 */
		public void testDarNumeroElementos()
		{
			setupEscenario1();
			int num = lista.darNumeroElementos();
			assertEquals("No se dio el n�mero correcto",8, num);
			lista.agregarElementoFinal("8");
			int num1 = lista.darNumeroElementos();
			assertEquals("No se dio el n�mero correcto",9, num1);
			lista.agregarElementoFinal("9");
			int num2 = lista.darNumeroElementos();
			assertEquals("No se dio el n�mero correcto",10, num2);
		}
		
		/**
		 * Realiza las pruebas sobre darElementoActual().
		 */
		public void testDarElemento()
		{
			setupEscenario1();
			String elemento = (String)lista.darElemento(1);
			assertEquals("No se dio el n�mero correcto","2", elemento);
			String elemento3 = (String)lista.darElemento(3);	
			assertEquals("No se dio el n�mero correcto","4", elemento3);
			String elemento8 = (String)lista.darElemento(7);	
			assertEquals("No se dio el n�mero correcto","7", elemento8);
		}
		
		public void testDarnumerodelementos()
		{
			setupEscenario1();
			int tama�o = lista.darNumeroElementos();
			assertEquals("no se dio el numero correcto",8, tama�o);
			lista.eliminarElemento(0);
			tama�o=lista.darNumeroElementos();
			assertEquals("no se dio el numero correcto",7, tama�o);
			lista.eliminarElemento(4);
			tama�o=lista.darNumeroElementos();
			assertEquals("no se dio el numero correcto",6, tama�o);
		}
		public void testeliminarElemento()
		{
			setupEscenario1();
			
			lista.eliminarElemento(0);
			String elemento = (String) lista.darElemento(0);
			assertEquals("2", elemento);
			lista.eliminarElemento(3);
			 elemento = (String) lista.darElemento(3);
			assertEquals("6", elemento);
		}

	
}
