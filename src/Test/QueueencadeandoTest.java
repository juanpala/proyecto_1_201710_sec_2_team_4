package Test;

import junit.framework.TestCase;
import model.data_structures.ListaDobleEncadenada;
import model.data_structures.Queueencadenado;

public class QueueencadeandoTest extends TestCase {

	
	Queueencadenado<String> lista ;
	
	public void setupEscenario1()
	{
		lista = new Queueencadenado<String>();
	    lista.enqueue("1");
	    lista.enqueue("2");
	    lista.enqueue("3");
	    lista.enqueue("4");
	    lista.enqueue("5");
	    lista.enqueue("6");
	    lista.enqueue("7");
	    lista.enqueue("8");
		
	}
	public void setupEscenario2()
	{
		lista = new Queueencadenado<String>();
		
	}
	
	
	public void testenqueue()
	{
		setupEscenario1();
	}
	
	public void testdequeue()
	{
		setupEscenario1();
		String m = lista.dequeue();
		assertEquals("fallo primera","1", m);
		m = lista.dequeue();
		assertEquals("fallo 2","2", m);
		m = lista.dequeue();
		assertEquals("fallo 3","3", m);
		m = lista.dequeue();
		assertEquals("fallo 4","4", m);
		m = lista.dequeue();
		assertEquals("fallo 5","5", m);
		m = lista.dequeue();
		assertEquals("fallo 6","6", m);
		m = lista.dequeue();
		assertEquals("fallo 7","7", m);
		m = lista.dequeue();
		assertEquals("fallo 8","8", m);
		m = lista.dequeue();
		assertEquals("fallo null",null, m);
	}
	public void testisEmpty()
	{
		setupEscenario2();
		boolean m = lista.isEmpty();
		assertEquals("la lista esta vacia",true, m);
		setupEscenario1();
		 m = lista.isEmpty();
		assertEquals("la lista no esta vacia",false, m);
	}
}

    


