package Test;

import java.util.Stack;

import junit.framework.TestCase;
import model.data_structures.ListaEncadenada;

public class ListaEncadenadaTest extends TestCase{

	//Atributos
	//Lista  encadenada sobre la que se realizaran las pruebas
	private ListaEncadenada lista;
	private Stack list;
	/**
	 * Declaraci�n del escenario con los datos sobre los que se trabajar�.
	 */
	
	public void setupEscenario1()
	{
		lista = new ListaEncadenada<>();
		lista.agregarElementoFinal("hola");
		lista.agregarElementoFinal("hola1");
		lista.agregarElementoFinal("hola2");
		lista.agregarElementoFinal("hola3");
	}
	public void setupEscenarios2()
	{
		list = new Stack();
		list.push("hola");
		list.push("hola1");
		list.push("hola2");
		list.push("hola3");
		list.push("hola4");
	}
	public void setupEscenarios3()
	{
		list = new Stack();

	}
	/**
	 * M�todo que realiza una prueba unitar�a sobre el m�todo numeroElementos en la clase Listaencadenada
	 */
	public void testdarNumeroElementos()
	{
		setupEscenario1();
	     int num =lista.darNumeroElementos();
		assertEquals("No se dio el numero correcto",4, num);
		lista.agregarElementoFinal("upss");
		num=lista.darNumeroElementos();
		assertEquals("No se dio el numero correcto",5, num);
	}
	/**
	 * M�todo que realiza el test correspondiente al m�todo darElmentos.
	 */
	public void testdarElemento()
	{
		setupEscenario1();
		String element = (String) lista.darElemento(1);
		
		assertEquals("no se esprava este incial","hola", element);
		        element = (String) lista.darElemento(3);
		assertEquals("no se enocntro lo esperado","hola2", element);
        element = (String) lista.darElemento(2);
		assertEquals("no se enocntro lo esperado","hola1", element);
        element = (String) lista.darElemento(4);
		assertEquals("no se enocntro lo esperado","hola3", element);
	}
	
	public void testeliminarelemento()
	{
		setupEscenario1();
		 String re = (String) lista.eliminarElemento(1);
		 assertEquals("no se elimino el correcto","hola", re);
		 re = (String) lista.eliminarElemento(2);
		 assertEquals("no era lo esperado","hola2", re);

	}
	
	public void testisEmpty()
	{
		setupEscenarios3();
		boolean  vacio = list.isEmpty();
		assertEquals("el arregro deveria estar vacio",true, vacio);
	}
	
	public void testpop()
	{
		setupEscenarios2();
		boolean  vacio = list.isEmpty();
		assertEquals("el arregro deveria estar vacio",false, vacio);
		String re = (String) list.pop();
		assertEquals("mal pop","hola4", re);
		int n = list.size();
		assertEquals("tama�o mal",4, n);
		 re = (String) list.pop();
		assertEquals("mal pop","hola3", re);
		 n = list.size();
		assertEquals("tama�o mal",3, n);
	}
	
}
