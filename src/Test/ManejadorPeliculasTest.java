package Test;

import junit.framework.TestCase;
import model.logic.ManejadorPeliculas;

public class ManejadorPeliculasTest<T> extends TestCase {
	

	ManejadorPeliculas man;

	

	public void setupEsenario1()
	{
		man = new ManejadorPeliculas();
		
	}
	public void setupEsenario2()
	{
		man = new ManejadorPeliculas();
  		man.cargarPeliculasSR("Data/movies.csv");
		man.cargarRatingsSR("Data/ratings.csv");
		man.cargarTagsSR("Data/tags.csv");
	}

	public void testcargarArchivoPeliculas()
	{
		setupEsenario1();
		boolean cargo = man.cargarPeliculasSR("Data/movies.csv");
		assertEquals("No se cargo el archivo",true, cargo);
	}
	
    public void testcargarRatingsSR()
    {
    	setupEsenario1();
    	man.cargarPeliculasSR("Data/movies.csv");
		boolean cargo = man.cargarRatingsSR("Data/ratings.csv");
		assertEquals("No se cargo el archivo",true, cargo);
    }
    public void testcargarTagsSR()
    {
    	setupEsenario1();
    	man.cargarPeliculasSR("Data/movies.csv");
		boolean cargo = man.cargarTagsSR("Data/tags.csv");
		assertEquals("No se cargo el archivo",true, cargo);
    }
    public void testsize()
    {
    	setupEsenario1();
    	man.cargarPeliculasSR("Data/movies.csv");
		man.cargarRatingsSR("Data/ratings.csv");
		man.cargarTagsSR("Data/tags.csv");
		int i = man.sizeMoviesSR();
		assertEquals(9125, i);
		i = man.sizeTagsSR();
		assertEquals(1296, i);
		i = man.sizeUsersSR();
		assertEquals(617, i);
    }
    public void test6b()
    {
    	setupEsenario2();
    	 man.tagsPeliculaSR(1704);
            
    }
    public void test3b()
    {
    	setupEsenario1();
    	man.cargarPeliculasSR("Data/movies.csv");
		man.cargarTagsSR("Data/tags.csv");
    	man.recomendarTagsGeneroSR(1);
    }
    public void test1b()
    {
    	setupEsenario2();
    	man.usuariosActivosSR(1);
    }
    public void test2b()
    {
    	setupEsenario2();
    	man.catalogoUsuariosOrdenadoSR();
    }
    public void test4b()
    {
    	setupEsenario2();
    	man.opinionTagsGeneroSR();
    }
}
