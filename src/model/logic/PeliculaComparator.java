package model.logic;

import api.Comparator;
import model.vo.VOPelicula;

public class PeliculaComparator implements Comparator<VOPelicula>
{

	/**
	 * M�todo encargado de las comparaciones entre objetos tipo VOPelicula
	 */
	public int compare(VOPelicula pel, VOPelicula peliComparada) 
	{
		if(pel.getAgnoPublicacion() > peliComparada.getAgnoPublicacion())
		{
			return 1;
		}
		else if(pel.getAgnoPublicacion() == peliComparada.getAgnoPublicacion())
		{
			if(pel.getPromedioRatings()>peliComparada.getPromedioRatings())
			{
				return 1;
			}
			else if(pel.getPromedioRatings()>peliComparada.getPromedioRatings())
			{
			//	return this.titulo.compareToIgnoreCase(pelComparar.titulo);
				return pel.getTitulo().compareToIgnoreCase(peliComparada.getTitulo());
			}
			else
			{
				return -1;
			}
		}
		else
		{
			return -1;
		}
	
	}

	
}
