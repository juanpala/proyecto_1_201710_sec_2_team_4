package model.logic;

import api.Comparator;
import model.vo.VOPelicula;

public class ComparadorpeliculasNumtags implements Comparator<VOPelicula> {

	@Override
	public int compare(VOPelicula a, VOPelicula b) {
		if(a.getTagsAsociados().darNumeroElementos()==b.getTagsAsociados().darNumeroElementos())
		return 0;
		else if(a.getTagsAsociados().darNumeroElementos()<b.getTagsAsociados().darNumeroElementos())
		return 1;
		else if(a.getTagsAsociados().darNumeroElementos()>b.getTagsAsociados().darNumeroElementos())
		return -1;
		
		return 0;
	}
	

}
