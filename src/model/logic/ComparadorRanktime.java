package model.logic;

import api.Comparator;
import model.vo.VORating;

public class ComparadorRanktime implements Comparator<VORating>{

	@Override
	public int compare(VORating a, VORating b) {
		if(a.getIdPelicula()==b.getIdPelicula())
			return 0;
			else if(a.getIdPelicula()<b.getIdPelicula())
			return 1;
			else if(a.getIdPelicula()>b.getIdPelicula())
			return -1;
		return 0;
	}

}
