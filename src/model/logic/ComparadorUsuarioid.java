package model.logic;

import api.Comparator;
import model.vo.VOTag;
import model.vo.VOUsuario;

public class ComparadorUsuarioid implements Comparator<VOUsuario> {

	@Override
	public int compare(VOUsuario a, VOUsuario b) {
	
			if(a.getIdUsuario()==b.getIdUsuario())
				return 0;
				else if(a.getIdUsuario()<b.getIdUsuario())
				return 1;
				else if(a.getIdUsuario()>b.getIdUsuario())
				return -1;
			return 0;
	}
	

}
