package model.logic;

import java.util.Comparator;

import model.vo.VORating;

public class ComparatorRatingsFechaCreacion implements Comparator<VORating>
{


	public int compare(VORating o1, VORating o2) 
	{
		double timeO1= o1.gettimestamp();
		double timeO2= o2.gettimestamp();
		if(timeO1==timeO2)
			return 0;
		if(timeO1>timeO2)
			return -1;
		else
			return 1;
	}

}
