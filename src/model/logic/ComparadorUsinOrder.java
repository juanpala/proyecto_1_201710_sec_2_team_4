package model.logic;

import api.Comparator;
import model.vo.VOUsuario;

public class ComparadorUsinOrder implements Comparator<VOUsuario> {

	@Override
	public int compare(VOUsuario a, VOUsuario b) {

        if(a.getPrimerTimestamp()==b.getPrimerTimestamp())
        {
        	if(a.getNumRatings()==b.getNumRatings())
        	{
        		if(a.getIdUsuario()==b.getIdUsuario())
        		{
        			return 0;
        		}
        		else if(a.getIdUsuario()<b.getIdUsuario())
        		{
        			return 1;
        			
        		}
        		else if (a.getIdUsuario()>a.getIdUsuario())
        		{
        			return -1;
        		}
        	}
        	else if(a.getNumRatings()<b.getNumRatings())
        	{
        		return 1;
        	}
        	else if(a.getNumRatings()>b.getNumRatings())
        			{
        		return -1;
        			}
        }
        else if(a.getPrimerTimestamp()<b.getPrimerTimestamp())
        {
        	return -1;
        }
        else if(a.getPrimerTimestamp()>b.getPrimerTimestamp())
        {
        	return 1;
        }
		return 0;
	}

}
