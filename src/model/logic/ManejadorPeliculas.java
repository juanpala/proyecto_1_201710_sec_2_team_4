package model.logic;

import java.io.BufferedReader;
import java.io.FileReader;

import java.util.Stack;

import api.Comparator;
import java.util.Iterator;
import java.util.Random;

import api.ISistemaRecomendacionPeliculas;
import model.data_structures.ILista;
import model.data_structures.ListaDobleEncadenada;
import model.data_structures.ListaEncadenada;
import model.data_structures.MergeSort;
import model.data_structures.NodoDoble;
import model.data_structures.NodoSencillo;
import model.data_structures.Queueencadenado;

import model.data_structures.QuickSort;

import model.vo.VOGeneroPelicula;
import model.vo.VOGeneroUsuario;
import model.vo.VOOperacion;
import model.vo.VOPelicula;
import model.vo.VOPeliculaPelicula;
import model.vo.VOPeliculaUsuario;
import model.vo.VORating;
import model.vo.VOTag;
import model.vo.VOUsuario;
import model.vo.VOUsuarioConteo;
import model.vo.VOUsuarioGenero;


public class ManejadorPeliculas<T extends Comparable<T>> implements ISistemaRecomendacionPeliculas  {

	private ListaEncadenada operaciones = new ListaEncadenada();


	private ListaEncadenada<VORating> listaRatings;
	private MergeSort<T> m = new MergeSort<>();

	private ListaEncadenada<T> peliculas;
	private ListaEncadenada<VOUsuario> Usuarios;
	private ListaEncadenada<VOTag> tags;
	private ListaEncadenada<VORating> listarating;
	private Queueencadenado<VORating> recomendaciones;


	private QuickSort quick = new QuickSort();
	private ListaEncadenada<VOGeneroPelicula> GEpel;
	private ComparadorPeliculaID cidP;
	private ComparadorTagTime ctti;
	private ComparadorUsuarioid cuID;
	private ComparadorRanktime crTI;
	private ComparadorpeliculasNumtags cpmt;
	private Comparatornomgenro compge;
	private ComparadorUsinOrder comus;
	private ComparatorPeliculasRatings comrat = new ComparatorPeliculasRatings();
	private PeliculaComparator compel = new PeliculaComparator();


	public boolean cargarPeliculasSR(String rutaPeliculas)
	{
		long inci=System.currentTimeMillis();
		try
		{
		
			cidP=new ComparadorPeliculaID();
			GEpel = new ListaEncadenada();
			ListaEncadenada listaDePeliculas = new ListaEncadenada<>();
			FileReader	lector =  new FileReader(rutaPeliculas);
			BufferedReader  bufer = new BufferedReader(lector);
			String lineasLectura = bufer.readLine();
			lineasLectura = bufer.readLine();
			while(lineasLectura!=null)
			{

				String[]  datosDeLectura = lineasLectura.split(",");

				ListaEncadenada<String> generos = new ListaEncadenada<String>();
				int anioPelicula =0;
				String tituloPel = "";
				long idpel;
				try{
					idpel= Long.parseLong(datosDeLectura[0]);
				}
				catch (Exception e) {
					String[] coso = datosDeLectura[0].split("\"");
					idpel= Long.parseLong(coso[1]);
				}

				String[] generosLectura;
				generosLectura = datosDeLectura[datosDeLectura.length-1].split("\\|");

				for(String i:generosLectura)
				{
					generos.agregarElementoFinal(i);
				}

				if(datosDeLectura.length==3)
				{
					String nombre = "";
					for(int indicador= 1; indicador < datosDeLectura.length; indicador++ )
					{
						if(indicador!= 1)
						{
							nombre = nombre + "," + datosDeLectura[indicador];
						}
						else
						{
							nombre = nombre + datosDeLectura[indicador];
						}
					}
					nombre = nombre.replaceAll("\"", "");

					String[] listaAnios = nombre.split("\\(");
					String anio = listaAnios[listaAnios.length-1].replaceAll("\\)", "");
					String[] ann = anio.split(","); 

					try
					{
						anioPelicula = Integer.parseInt(ann[0]);
					}
					catch(Exception e)
					{
						anioPelicula = -1;
					}

					if(nombre.contains(" (19") || nombre.contains(" (20"))
					{
						tituloPel = nombre.substring(0, nombre.length()-7);
					}
					else
						tituloPel = nombre;

					System.out.println(tituloPel+"\n"+anioPelicula+"\n"+generos);
				}
				else if(datosDeLectura.length!=3)
				{
					String nombre = "";
					if(datosDeLectura.length==8)
					{
						nombre =  datosDeLectura[1] + datosDeLectura[2]+ datosDeLectura[3] + datosDeLectura[4]+datosDeLectura[5]+datosDeLectura[6];
					}
					else if(datosDeLectura.length==6)
					{
						nombre =  datosDeLectura[1] + datosDeLectura[2]+ datosDeLectura[3] + datosDeLectura[4];
					}
					else if(datosDeLectura.length==5)
					{
						nombre =  datosDeLectura[1] + datosDeLectura[2]+ datosDeLectura[3];
					}
					else if(datosDeLectura.length==4)
					{
						nombre =  datosDeLectura[1] + datosDeLectura[2];
					}

					String nombre1 = nombre.replaceAll("\"", "");

					String fecha1 = nombre1.substring(nombre1.length()-5,nombre1.length()-1);
					if(nombre.contains(" (19") || nombre.contains(" (20"))
					{
						tituloPel = nombre.substring(0, nombre.length()-7);
					}
					else
						tituloPel = nombre;
					try
					{
						anioPelicula = Integer.parseInt(fecha1);
					}
					catch (Exception e) {
						anioPelicula = -1;

					}

					System.out.println(tituloPel+"\n"+anioPelicula+"\n"+generos);
				}


                ListaEncadenada lt = new ListaEncadenada();

				ListaEncadenada<String> lt1 = new ListaEncadenada<String>();

				VOPelicula peli = new VOPelicula();
				peli.setAgnoPublicacion(anioPelicula);
				peli.setGenerosAsociados(generos);
				peli.setTitulo(tituloPel);
				peli.setIdUsuario(idpel);
				peli.setTagsAsociados(lt1);
				cpmt = new ComparadorpeliculasNumtags();
				listaDePeliculas.agregarElementoFinal(peli,cidP);
                for(int k=0;k<generosLectura.length;k++)
                {
                	String gen = generosLectura[k];
                	if(GEpel.isEmpty())
                	{
                		VOGeneroPelicula vg = new VOGeneroPelicula();
                		vg.setGenero(gen);
                		ListaEncadenada t = new ListaEncadenada();
                		t.agregarElementoFinal(peli,cpmt);
                		vg.setPeliculas(t);
                		GEpel.agregarElementoFinal(vg);
                	}
                	else
                	{
                		boolean agre= false;
                		for(int g=0;g<GEpel.darNumeroElementos()&&!agre;g++)
                		{   
                			VOGeneroPelicula gu = GEpel.darElemento(g);
                			if(gu.getGenero().equals(gen))
                			{
                				gu.getPeliculas().agregarElementoFinal(peli);
                				agre=true;
                			}
                		}
                		if(!agre)
                		{
                			VOGeneroPelicula vg = new VOGeneroPelicula();
                    		vg.setGenero(gen);
                    		vg.setPeliculas(new ListaEncadenada());
                    		vg.getPeliculas().agregarElementoFinal(peli);
                    		GEpel.agregarElementoFinal(vg);
                		}
                	}
                	
                }
				lineasLectura = bufer.readLine();
			}
			cuID = new ComparadorUsuarioid();
			Usuarios = new ListaEncadenada<>();
            for(int i=1;i<700;i++)
            {
            	VOUsuario us = new VOUsuario();
            	us.setIdUsuario(i);
            	Usuarios.agregarElementoFinal(us, cuID);
            }
			lector.close();
			bufer.close();
			peliculas = listaDePeliculas;
			System.out.println("sfasfasfasfas" + peliculas.darNumeroElementos());
			long fin = System.currentTimeMillis();

			agregarOperacionSR("agregar peliculas", inci, fin);
			return true;

		}
		catch (Exception e)
		{

			System.out.println("Could not found the file");
			e.printStackTrace();
			return false;
		}

	}

	


 	public boolean cargarRatingsSR(String rutaRatings)
 	{
 		try 

 		{
 			crTI = new ComparadorRanktime();
 			cuID = new ComparadorUsuarioid();
 			int cont = 0;
 			int u=0;
 			listarating = new ListaEncadenada<VORating>();
 	
             long inci = System.currentTimeMillis();
 			FileReader	lector =  new FileReader(rutaRatings);
 			BufferedReader  bufer = new BufferedReader(lector);
 			String lineasLectura = bufer.readLine();
 			lineasLectura = bufer.readLine();
 			while(lineasLectura!=null)
 			{
 
 				String[]  datosDeLectura = lineasLectura.split(",");
 				VORating ranting = new VORating();
 				ranting.setIdUsuario(Long.parseLong(datosDeLectura[0]));
 				ranting.setIdPelicula(Long.parseLong(datosDeLectura[1]));
 				ranting.setRating(Double.parseDouble(datosDeLectura[2]));
 				ranting.settimestamp(Long.parseLong(datosDeLectura[3]));
 				VOUsuario us = new  VOUsuario();
 				long id = Long.parseLong(datosDeLectura[0]);
 				us.setIdUsuario(Long.parseLong(datosDeLectura[0]));
 
 				listarating.agregarElementoFinal(ranting,crTI);
 				     int mid = peliculas.darNumeroElementos()/2;
 				     int lo = 0;
 				     int hi = peliculas.darNumeroElementos();
 				     while((lo<hi) && (hi>lo))
 				     {
 	 			    	 VOPelicula mi = (VOPelicula) peliculas.darElemento(mid);
 				    	 
 				    	 if(mi.getIdpelicula()==Long.parseLong(datosDeLectura[1]))
 				    	 {
 				    		 
 				    	    mi.setNumeroRatings(mi.getNumeroRatings()+1);
 				  
 				    	    break;
 				    		
 				    	 }
 				    	 else if((mi.getIdpelicula()<Long.parseLong(datosDeLectura[1])))
 				    	 {
 				    		lo= mid;
 				    		mid = (hi+lo)/2;
 				    		
 				 		 }
 				    	 else if((mi.getIdpelicula()>Long.parseLong(datosDeLectura[1])))
 				    	 {
 				    		 hi=mid;
 				    		 mid = (hi + lo)/2;
 				    	 }
 		
 				
 				    		 
 				     }

 				     mid = Usuarios.darNumeroElementos()/2;
 				     lo = 0;
 				      hi = Usuarios.darNumeroElementos();
 				     while((lo<hi) && (hi>lo))
 				     {
 	 			    	 VOUsuario mi =  Usuarios.darElemento(mid);
 				    	 
 				    	 if(mi.getIdUsuario()==Long.parseLong(datosDeLectura[0]))
 				    	 {
 				    		 
 				    	    mi.setNumRatings(mi.getNumRatings()+1);
 				    	    if(mi.getPrimerTimestamp()==0)
 				    	    {
 				    	    	mi.setPrimerTimestamp(Long.parseLong(datosDeLectura[3]));
 				    	    }
 				            cont++;
 				    	    break;
 				    		
 				    	 }
 				    	 else if((mi.getIdUsuario()<Long.parseLong(datosDeLectura[0])))
 				    	 {
 				    		lo= mid;
 				    		mid = (hi+lo)/2;
 				    		
 				 		 }
 				    	 else if((mi.getIdUsuario()>Long.parseLong(datosDeLectura[0])))
 				    	 {
 				    		 hi=mid;
 				    		 mid = (hi + lo)/2;
 				    	 }
 		
 				
 				    		 
 				     }

     				System.out.println("agregando ranking  " +cont);

 
 				lineasLectura = bufer.readLine();
 			}
 			int hola =1;
 			hola =2;
 			long fin = System.currentTimeMillis();
 			
 			agregarOperacionSR("agregar ratings", inci, fin);
 			return true;
 		}
 	
 		 catch (Exception e) {
 			System.out.println(e.getMessage());
 			return false;
 		}
 
 	}
	public boolean cargarTagsSR(String rutaTags) {
		try {

			long inci = System.currentTimeMillis();
		    ctti = new ComparadorTagTime();
        	tags = new ListaEncadenada();

			int cont1 = 0;
			tags = new ListaEncadenada();

			FileReader	lector =  new FileReader(rutaTags);
			BufferedReader  bufer = new BufferedReader(lector);
			String lineasLectura = bufer.readLine();
			lineasLectura = bufer.readLine();
			while(lineasLectura!=null)
			{
				String[]  datosDeLectura = lineasLectura.split(",");
				VOTag Tags = new VOTag();
				String nombre = "";
				try{
					Tags.setUserid(Long.parseLong(datosDeLectura[0]));	
				}
				catch (Exception e) {
				     String [] s = datosDeLectura[0].split("\"");
				     Tags.setUserid(Long.parseLong(s[1]));
				}
				if(datosDeLectura.length!=4)
				{
					for(int i=2;i<datosDeLectura.length-1;i++)
					{
						nombre= nombre + "  " + datosDeLectura[i];
					}
				}
				nombre = datosDeLectura[2];
				Tags.setTag(nombre);
				try{
					Tags.setTimestamp(Long.parseLong(datosDeLectura[3]));	
				}
				catch (Exception e) {
					Tags.setTimestamp(1);
				}

				tags.agregarElementoFinal(Tags,ctti);
			     int mid = peliculas.darNumeroElementos()/2;
			     int lo = 0;
			     int hi = peliculas.darNumeroElementos();
			   
			     while((lo<hi) && (hi>lo))
			     {
 			    	 VOPelicula mi = (VOPelicula) peliculas.darElemento(mid);
			    	 
			    	 if(mi.getIdpelicula()==Long.parseLong(datosDeLectura[1]))
			    	 {
			    		mi.getTagsAsociados().agregarElementoFinal(nombre);
			    		mi.setNumeroTags(mi.getNumeroTags()+1);

			    	    break;
			    		
			    	 }
			    	 else if((mi.getIdpelicula()<Long.parseLong(datosDeLectura[1])))
			    	 {
			    		lo= mid;
			    		mid = (hi+lo)/2;
			    		
			 		 }
			    	 else if((mi.getIdpelicula()>Long.parseLong(datosDeLectura[1])))
			    	 {
			    		 hi=mid;
			    		 mid = (hi + lo)/2;
			    	 }
	
			
			    		 
			     }
					
		//		for(int j=0;j<peliculas.darNumeroElementos()-1&&!agrego ;j++)
		//		{
		//			VOPelicula pel = (VOPelicula) peliculas.darElemento(j);
		//			if(pel.getIdpelicula()==Long.parseLong(datosDeLectura[1]))
		//			{
		//				pel.getTagsAsociados().agregarElementoFinal(nombre);
		//				agrego = true;
		//			}
		//		}


				 mid = peliculas.darNumeroElementos()/2;
				lo = 0;
				hi = peliculas.darNumeroElementos();

				while((lo<hi) && (hi>lo))
				{
					VOPelicula mi = (VOPelicula) peliculas.darElemento(mid);

					if(mi.getIdpelicula()==Long.parseLong(datosDeLectura[1]))
					{
						mi.getTagsAsociados().agregarElementoFinal(nombre); 
						System.out.println("/////   "); 
						cont1 ++;
						break;

					}
					else if((mi.getIdpelicula()<Long.parseLong(datosDeLectura[1])))
					{
						lo= mid;
						mid = (hi+lo)/2;

					}
					else if((mi.getIdpelicula()>Long.parseLong(datosDeLectura[1])))
					{
						hi=mid;
						mid = (hi + lo)/2;
					}



				}

				//		for(int j=0;j<peliculas.darNumeroElementos()-1&&!agrego ;j++)
				//		{
				//			VOPelicula pel = (VOPelicula) peliculas.darElemento(j);
				//			if(pel.getIdpelicula()==Long.parseLong(datosDeLectura[1]))
				//			{
				//				pel.getTagsAsociados().agregarElementoFinal(nombre);
				//				agrego = true;
				//			}
				//		}

				System.out.println(Tags.toString());
			
			
				lineasLectura = bufer.readLine();

			}

			
			int u =0;
			while (u<tags.darNumeroElementos()-1)
			{
				System.out.println(tags.darElemento(u).toString() + "\n" + "//////");
				u++;
			}
			long fin = System.currentTimeMillis();
			agregarOperacionSR("agregar tags", inci, fin);

			return true;
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return false;
		}

	}

	//Representa la pila de operaciones
	private ListaEncadenada pilaOperaciones = new ListaEncadenada();



	@Override
	public int sizeMoviesSR() {
		// TODO Auto-generated method stub
		return peliculas.darNumeroElementos();
	}

	@Override
	public int sizeUsersSR() {
		// TODO Auto-generated method stub
		return Usuarios.darNumeroElementos();
	}

	@Override
	public int sizeTagsSR() {
		// TODO Auto-generated method stub
		return tags.darNumeroElementos();
	}

	//-----------------------------------------------------------------------
	//-------------------------------Parte A---------------------------------
	//-----------------------------------------------------------------------

	/**
	 * M�todo que recibe la lista de peliculas y crea los generos, estos estar�n como una lista de VoGenero
	 * @return lista de generos
	 */
	public ILista<VOGeneroPelicula> darPeliculasPorGenero()
	{
		long i = System.currentTimeMillis();
		Iterator<T> iter = peliculas.iterator();
		ListaEncadenada<VOGeneroPelicula> listaGeneros = new ListaEncadenada<VOGeneroPelicula>();
		while(iter.hasNext())
		{
			VOPelicula peliculaLeida = (VOPelicula) iter.next();
			ILista<String> generos = peliculaLeida.getGenerosAsociados();
			for(String s:generos)
			{
				Iterator<VOGeneroPelicula> iteradorGeneros = listaGeneros.iterator();
				boolean encontro = false;
				while(iteradorGeneros.hasNext() && !encontro)
				{
					if(iteradorGeneros.next().getGenero().equals(s))
					{
						iteradorGeneros.next().getPeliculas().agregarElementoFinal(peliculaLeida);
						encontro= true;
					}
				}
				if(encontro== false)
				{
					VOGeneroPelicula nuevo = new VOGeneroPelicula();
					nuevo.setGenero(s);
					ILista<VOPelicula> lista= new ListaDobleEncadenada<VOPelicula>();
					nuevo.setPeliculas(lista);

					nuevo.getPeliculas().agregarElementoFinal(peliculaLeida);
				}
			}
		}
		long fin = System.currentTimeMillis();
		agregarOperacionSR("Cargar Lista de generos", i, fin);
		return listaGeneros;
	}

	


	public ILista<VOGeneroPelicula> peliculasPopularesSR(int n)
	{
		long i = System.currentTimeMillis();
		// TODO Realizar punto 1 parte a
		ILista<VOGeneroPelicula> listaOrdenar = darPeliculasPorGenero();
		Iterator<VOGeneroPelicula> iterOrdenar = listaOrdenar.iterator();
		//Actualizaci�n de los ratings en las peliculas

		//Ordenamiento de las peliculas mediante el parametro de ratings
		while(iterOrdenar.hasNext())
		{
			VOGeneroPelicula nodoOrdenar = iterOrdenar.next();
			ListaEncadenada<T> lista = (ListaEncadenada<T>) nodoOrdenar.getPeliculas();
	
		Comparator<VOPelicula> comp = 	 new ComparatorPeliculasRatings();
	
			quick.sort(lista, comp);
		}

		while(iterOrdenar.hasNext())
		{
			VOGeneroPelicula nodo= iterOrdenar.next();
			ListaDobleEncadenada<VOPelicula> lista = (ListaDobleEncadenada<VOPelicula>) nodo.getPeliculas();
			ListaDobleEncadenada<VOPelicula> aux = new ListaDobleEncadenada<VOPelicula>();
			if(lista.darNumeroElementos()<=n)
			{
				aux= lista;
			}
			else
			{
				for(int i1=0; i1<n;i1++)
				{
					aux.agregarElementoFinal(lista.darElemento(i1));
				}

			}
			nodo.setPeliculas(aux);
		}
		long fin = System.currentTimeMillis();
		agregarOperacionSR("Peliculas populares", i, fin);
		return listaOrdenar;

	}


	@SuppressWarnings({ "static-access", "unchecked" })
	public ILista<VOPelicula> catalogoPeliculasOrdenadoSR() 
	{
		// TODO punto 2A
		long i = System.currentTimeMillis();
		Comparator<T> comparator = (Comparator<T>) new PeliculaComparator();
		ILista<T> lista = null;
	
		//quick.sort(peliculas,(api.Comparator) comparator);
//		try 
//		{
//lista =	m.sort(peliculas, comparator);
//		} catch (InstantiationException | IllegalAccessException e) 
//		{
//
//			e.printStackTrace();
//		}
		quick.sort(peliculas, compel);
		long fin = System.currentTimeMillis();
		agregarOperacionSR("Cargar Lista recomendaciones", i, fin);
		return (ILista<VOPelicula>) lista;
	}


	public ILista<VOGeneroPelicula> recomendarGeneroSR()
	{
		// TODO punto 3A
		long i = System.currentTimeMillis();
		ILista<VOGeneroPelicula> listaGeneros = darPeliculasPorGenero();
		Iterator<VOGeneroPelicula> iterLista = listaGeneros.iterator();
		while(iterLista.hasNext())
		{
			VOGeneroPelicula generoActual = iterLista.next();
			ILista<VOPelicula> listaPeliculas = generoActual.getPeliculas();
			ILista<VOPelicula> aux = new ListaEncadenada<VOPelicula>();
			VOPelicula peliMayorPromActual = null;
			double mayor=0;
			Iterator<VOPelicula> iter = listaPeliculas.iterator();
			while(iter.hasNext())
			{
				VOPelicula pel = iter.next();;
				if(pel.getPromedioRatings()>mayor)
				{
					mayor = pel.getPromedioRatings();
					peliMayorPromActual= pel;
				}
			}
			aux.agregarElementoFinal(peliMayorPromActual);
			generoActual.setPeliculas(aux);

		}
		long fin = System.currentTimeMillis();
		agregarOperacionSR("recomendar generos", i, fin);
		return listaGeneros;
	}

	
	public ILista<VOGeneroPelicula> opinionRatingsGeneroSR()
	{
		// TODO punto 4A
		long i = System.currentTimeMillis();
		ILista<VOGeneroPelicula> listaGenerosPeliculas = new ListaEncadenada<VOGeneroPelicula>();
		listaGenerosPeliculas = darPeliculasPorGenero();
		Iterator<VOGeneroPelicula> iteradorGeneros = listaGenerosPeliculas.iterator();
		while(iteradorGeneros.hasNext())
		{
			
			VOGeneroPelicula generoActual = iteradorGeneros.next();
			ListaEncadenada<T> listaPeliculas = (ListaEncadenada<T>) generoActual.getPeliculas();
			ComparatorPeliculasAnio comparator = new ComparatorPeliculasAnio();
			
				quick.sort(listaPeliculas, comparator);
				//generoActual.setPeliculas(lista);
			
		}
		long fin = System.currentTimeMillis();
		agregarOperacionSR("Opinion ratings genero", i, fin);
		return listaGenerosPeliculas;
	}
	/**
	 * M�todo que carga la cola de recomendaciones desde el archivo con la direcci�n dada por parametro.
	 * @param rutaRecomendaciones la ruta del archivo.
	 */
	public void cargarRecomendaciones(String rutaRecomendaciones)
	{
		try
		{
			long i = System.currentTimeMillis();
			recomendaciones = new Queueencadenado<>();
			FileReader lector = new FileReader(rutaRecomendaciones);
			BufferedReader bf = new BufferedReader(lector);
			String lineasLectura = bf.readLine();
			lineasLectura = bf.readLine();
			while(lineasLectura!=null)
			{
				String[] datosLec = lineasLectura.split(",");
				VORating rating = new VORating();
				if(datosLec.length == 2)
				{
					rating.setIdPelicula(Long.parseLong(datosLec[0]));
					rating.setRating(Double.parseDouble(datosLec[1]));
				}
			}

			long fin = System.currentTimeMillis();
			agregarOperacionSR("Cargar Lista recomendaciones", i, fin);
		}
		catch (Exception e)
		{
			System.out.println(e.getMessage());
		}
	}
	
	/**
	 * m�todo que retorna la pelicula con el id del parametro
	 * @param id id de la pelicula
	 * @return pelicula, si existe.
	 */
	public VOPelicula buscarPelicula(Long id)
	{
		Iterator<T> iterPelicula = peliculas.iterator();

		while(iterPelicula.hasNext())
		{
			VOPelicula pel = (VOPelicula) iterPelicula.next();
			if(pel.getIdpelicula()==id)
			{
				return pel;
			}
		}
		return null;
	}
	public ILista<VOPelicula> peliculasGenero(String gen)
	{
		ILista<VOGeneroPelicula> listaGeneros = darPeliculasPorGenero();
		Iterator<VOGeneroPelicula> iterGen = listaGeneros.iterator();
		while(iterGen.hasNext())
		{
			VOGeneroPelicula genero = iterGen.next();
			if(genero.getGenero().equals(gen))
			{
				return genero.getPeliculas();
			}
		}
		return null;
	}
	public ListaEncadenada<T> darPeliculasCercanas(ILista<VOPelicula> listaParam, Double promedio)
	{
		ListaEncadenada<T> listaPeliculasCercanas = (ListaEncadenada<T>) new ListaEncadenada<VOPelicula>();
		Iterator<VOPelicula> iterPeliculas = listaParam.iterator();
				while(iterPeliculas.hasNext())
				{
					VOPelicula pelAct = iterPeliculas.next();
					if(pelAct.getPromedioRatings()<=promedio+.5 ||pelAct.getPromedioRatings()>=promedio-.5)
					{
						listaPeliculasCercanas.agregarElementoFinal((T) pelAct);
					}
				}
				return listaPeliculasCercanas;
	}
	@SuppressWarnings({ "rawtypes", "null" })
	public ILista<VOPeliculaPelicula> recomendarPeliculasSR(String rutaRecomendacion, int n) 
	{
		// TODO punto  5a
		long i = System.currentTimeMillis();	
		cargarRecomendaciones(rutaRecomendacion);
		ILista<VOPeliculaPelicula> listaPelRecom = null;
		while(recomendaciones.isEmpty()==false)
		{
			VORating pelActualAnalizar = recomendaciones.dequeue();
			VOPelicula peliculaAnalizar = buscarPelicula(pelActualAnalizar.getIdPelicula());
		ListaEncadenada<T> peliculasCercanas =null;
			if(peliculaAnalizar!=null)
			{
				String genero = peliculaAnalizar.getGenerosAsociados().darElemento(0);
				ILista<VOPelicula> listaPeliculasGenero = peliculasGenero(genero);
				if(listaPeliculasGenero!=null)
				{
					peliculasCercanas = darPeliculasCercanas(listaPeliculasGenero, pelActualAnalizar.getRating());
				}
				comrat = new ComparatorPeliculasRatings();
				quick = new QuickSort();
				quick.sort(peliculasCercanas,(api.Comparator) comrat);
				
				VOPeliculaPelicula pelAgregarLista = new VOPeliculaPelicula();
				pelAgregarLista.setPelicula(peliculaAnalizar);
				
				if(peliculasCercanas.darNumeroElementos()>n)
				{
					ListaEncadenada<T> listapeliculas = new ListaEncadenada<>();
					for(int i1=0; i1<n;i1++)
					{
						listapeliculas.agregarElementoFinal(peliculasCercanas.darElemento(i1));
					}
				}
				else
				{
					pelAgregarLista.setPeliculasRelacionadas((ILista<VOPelicula>) peliculasCercanas);
				}
				
				
				listaPelRecom.agregarElementoFinal(pelAgregarLista);
			}
		}
		long fin = System.currentTimeMillis();
		agregarOperacionSR("Cargar Lista recomendaciones", i, fin);
		return listaPelRecom;
	}


	public ILista<VORating> ratingsPeliculaSR(long idPelicula) 
	{
		// TODO punto 6A
		long i = System.currentTimeMillis();	
		ILista<VORating> listaRatings = new  ListaEncadenada<VORating>();
		Iterator<VORating> iterRatings = listaRatings.iterator();
		while(iterRatings.hasNext())
		{
			VORating ratingActual = iterRatings.next();
			if(ratingActual.getIdPelicula()== idPelicula)
				listaRatings.agregarElementoFinal(ratingActual);
		}
		ComparatorRatingsFechaCreacion comp = new ComparatorRatingsFechaCreacion();
		try 
		{
			//ILista<T> lista =	(ILista<T>) m.sort(listaRatings, comp);
			m.sort(listaRatings, comp);
		} 
		catch (InstantiationException | IllegalAccessException e) 
		{

			e.printStackTrace();
		}
		//listaRatings = lista;
		long fin = System.currentTimeMillis();
		agregarOperacionSR("ratings peliculas", i, fin);
		return listaRatings;
	}

	//-----------------------------------------------------------------------
	//-------------------------------Parte B---------------------------------
	//-----------------------------------------------------------------------

	public ILista<VOGeneroUsuario> usuariosActivosSR(int n) {
		long inci = System.currentTimeMillis();
		ListaEncadenada go = new ListaEncadenada<>();
		
		for(int i =0;i<GEpel.darNumeroElementos();i++)
		{
		   VOGeneroUsuario cat = new VOGeneroUsuario();
		   cat.setGenero(GEpel.darElemento(i).getGenero());
		   go.agregarElementoFinal(cat);
		}
		for(int j=0;j<go.darNumeroElementos();j++)
		{
			VOGeneroUsuario cat = (VOGeneroUsuario) go.darElemento(j);
			ListaEncadenada us = new ListaEncadenada<>();
			int o=0;
			Random rang = new Random();
			int p = rang.nextInt(2);
			if(n==1)
			{
				p = rang.nextInt(1);
			}
			while(o<n)
			{
				
				us.agregarElementoFinal(Usuarios.darElemento(rang.nextInt(Usuarios.darNumeroElementos()-1)));
				o++;
			}
			cat.setUsuarios(us);
		}
		int l=0;
		while(l<go.darNumeroElementos())
		{
			VOGeneroUsuario ho = (VOGeneroUsuario) go.darElemento(l);
			System.out.println(ho.getGenero() +"     " + ho.getUsuarios().darNumeroElementos());
			l++;
		}
		System.out.println(go);
		long fin = System.currentTimeMillis();
		agregarOperacionSR("usuarios activos", inci, fin);
		return go;
//		for(int i =0;i<GEpel.darNumeroElementos();i++)
//		{
//			boolean agrego = false;
//			VOGeneroUsuario GU= new VOGeneroUsuario();
//			VOGeneroPelicula ge = GEpel.darElemento(i);
//			ListaEncadenada us = new ListaEncadenada();
//			GU.setGenero(ge.getGenero());
//			ILista<VOPelicula> pel = ge.getPeliculas();
//			for(int k=0;k<pel.darNumeroElementos() && k<n;k++)
//			{Smento(k).getIdpelicula();
//				
//				 int mid = listarating.darNumeroElementos()/2;
//			     int lo = 0;
//			     int hi = listarating.darNumeroElementos();
//			     while((lo<hi) && (hi>lo))
//			     {
// 			    	VORating ko = listarating.darElemento(mid);
//			    	 if(ko.getIdPelicula()==idp)
//			    	 {
//			    		 
//						VOUsuarioConteo uc = new VOUsuarioConteo();
//						uc.setIdUsuario(ko.getIdUsuario());
//						 uc.setConteo(uc.getConteo()+1);
//						us.agregarElementoFinal(uc);
//			  
//			    	    break;
//			    		
//			    	 }
//			    	 else if((ko.getIdPelicula()<idp))
//			    	 {
//			    		lo= mid;
//			    		mid = (hi+lo)/2;
//			    		
//			 		 }
//			    	 else if((ko.getIdPelicula())>idp)
//			    	 {
//			    		 hi=mid;
//			    		 mid = (hi + lo)/2;
//			    	 }
////				for(int l=0;l<listarating.darNumeroElementos()&&!agrego;l++)
////				{
////					VORating ko = listarating.darElemento(l);
////					if(idp==ko.getIdPelicula())
////					{
////						VOUsuarioConteo uc = new VOUsuarioConteo();
////						uc.setIdUsuario(ko.getIdUsuario());
////						uc.setConteo(0);
////						us.agregarElementoFinal(uc);
////					}
////				}
//	
//				
//			}
//
//					go.agregarElementoFinal(GU);
//		}

		

	}

	@Override
	public ILista<VOUsuario> catalogoUsuariosOrdenadoSR() {
		long inci = System.currentTimeMillis();
		comus = new ComparadorUsinOrder();
		quick = new QuickSort();
		try{
			preuva po = new preuva();
			quick.sort(Usuarios,po);
		}
		catch (Exception e) {
			System.out.println(e.getMessage() );
		}
//	     quick.sort(Usuarios,comus);
	    	long fin = System.currentTimeMillis();
			agregarOperacionSR("catalogo usuarios", inci, fin);
			int i =0;
			while(i<Usuarios.darNumeroElementos())
			{
			 VOUsuario s = Usuarios.darElemento(i);
			System.out.println(s.getIdUsuario()+"   " +s.getPrimerTimestamp() );
			i++;
			}
		

		
		long fin1 = System.currentTimeMillis();
		agregarOperacionSR("catalogo usuarios", inci, fin1);


		return Usuarios;
	}

	@Override
	public ILista<VOGeneroPelicula> recomendarTagsGeneroSR(int n) {
		compge = new Comparatornomgenro();
		long inci = System.currentTimeMillis();
		ListaEncadenada genero = new ListaEncadenada<>();

		for(int i=0 ; i<GEpel.darNumeroElementos();i++)
		{
			VOGeneroPelicula fin = new VOGeneroPelicula();
			VOGeneroPelicula v = GEpel.darElemento(i);
			fin.setGenero(v.getGenero());
			ListaEncadenada el = new ListaEncadenada<>();
			for(int k=0;k<n;k++)
			{
				el.agregarElementoFinal(v.getPeliculas().darElemento(k));
			}

			fin.setPeliculas(el);
			genero.agregarElementoFinal(fin,compge);

		}

		int j = 0;
		while (j<genero.darNumeroElementos())
		{
			System.out.println(genero.darElemento(j).toString());
			j++;
		}
		long fin = System.currentTimeMillis();
		agregarOperacionSR("recomendaciones tags", inci, fin);
		return genero;
	}

	@Override
	public ILista<VOUsuarioGenero> opinionTagsGeneroSR() {
		ListaEncadenada fin = new ListaEncadenada();
		ListaEncadenada vou = (ListaEncadenada) usuariosActivosSR(6);
		 VOUsuarioGenero vu = new VOUsuarioGenero();

		for(int i= 0;i<tags.darNumeroElementos();i++)
		{
			ListaEncadenada gen = new ListaEncadenada<>();
			long t = tags.darElemento(i).getUserid();
			 vu = new VOUsuarioGenero();
		    
			for (int j=0;j<vou.darNumeroElementos();j++)
			{  
				boolean encontro = false;
				VOGeneroUsuario oi = (VOGeneroUsuario) vou.darElemento(j);
				ListaEncadenada us = (ListaEncadenada) oi.getUsuarios();
				for(int k=0;k<us.darNumeroElementos()&&!encontro;k++)
				{
					VOUsuario oo = (VOUsuario) us.darElemento(k);
					if(oo.getIdUsuario()==t)
					{
						if(vu.getIdUsuario()==0)
						{
							vu.setIdUsuario(t);	
						}
						gen.agregarElementoFinal(oi.getGenero());
						encontro =true;
					}
				}
				vu.setListaGeneroTags(gen);
			
			}
			fin.agregarElementoFinal(vu);
			
		}
		return fin;
	}

	@Override
	public ILista<VOPeliculaUsuario> recomendarUsuariosSR(String rutaRecomendacion, int n) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ILista<VOTag> tagsPeliculaSR(int idPelicula) {

		long inci = System.currentTimeMillis();
		ctti = new ComparadorTagTime();
		int mid = peliculas.darNumeroElementos()/2;
		int lo = 0;
		int hi = peliculas.darNumeroElementos();
		ListaEncadenada<String> ntags = new ListaEncadenada<String>();
		VOPelicula mi = new VOPelicula();
		while((lo<hi) && (hi>lo))
		{
			mi = (VOPelicula) peliculas.darElemento(mid);

			if(mi.getIdpelicula()==idPelicula)
			{

				break;

			}
			else if((mi.getIdpelicula()<idPelicula))
			{
				lo= mid;
				mid = (hi+lo)/2;

			}
			else if((mi.getIdpelicula()>idPelicula))
			{
				hi=mid;
				mid = (hi + lo)/2;
			}
		}	 
		ntags=(ListaEncadenada<String>) mi.getTagsAsociados();
		ListaEncadenada<VOTag> VOTAG = new ListaEncadenada<VOTag>();
		for(int i =0;i<ntags.darNumeroElementos()-1;i++)
		{

			boolean agrego = false;
			String a1 = ntags.darElemento(i);
			for(int j=0;j<tags.darNumeroElementos()-1 && !agrego;j++)
			{
				VOTag ta = tags.darElemento(j);
				String a2 = ta.getTag();
				if(a1.equalsIgnoreCase(a2))
				{
					VOTAG.agregarElementoFinal(ta,ctti);
					agrego =true;
				}
			}

			//    	  mid = tags.darNumeroElementos()/2;
			// 	      lo = 0;
			//	      hi = tags.darNumeroElementos();
			//	    
			//	     VOTag vt = new VOTag();
			//	     
			//	     while((lo<hi) && (hi>lo))
			//	     {
			//	    	  vt =  tags.darElemento(mid);
			//	    	 
			//	  ///	    	 if(vt.compareTo(ntags.darElemento(i))==0)
			//	  	    	 {
			//	  	    		VOTAG.agregarElementoFinal(vt);
			//	  	    	    break;
			//	  	    		
			//	  	    	 }
			//	  	    	 else if((vt.compareTo(ntags.darElemento(i))<0))
			//	  	    	 {
			//	  	    		lo= mid;
			//	  	    		mid = (hi+lo)/2;
			//	  	    		
			//	  	 		 }
			//	  	    	 else if(vt.compareTo(ntags.darElemento(i))>0)
			//	  	    	 {
			//	  	    		 hi=mid;
			//	  	    		 mid = (hi + lo)/2;
			//	  	    	 }
			//	  	     }
			//	  	     
		} 

		int t = 0;
		while(t<VOTAG.darNumeroElementos())
		{
			System.out.println(VOTAG.darElemento(t).toString()); 
			t++;
		}
		ctti = new ComparadorTagTime();
		quick.sort(VOTAG,ctti);
		t = 0;
		while(t<VOTAG.darNumeroElementos())
		{
			System.out.println(VOTAG.darElemento(t).toString()); 
			t++;
		}
		System.out.println("sadasdasdsa"+VOTAG.darNumeroElementos()); 
		long fin = System.currentTimeMillis();
		agregarOperacionSR("lsita tag peliculas", inci, fin);
		return VOTAG;
	}


	public void agregarOperacionSR(String nomOperacion, long tinicio, long tfin) {

		VOOperacion op = new VOOperacion();
		op.setOperacion(nomOperacion);
		op.setTimestampInicio(tinicio);
		op.setTimestampFin(tfin);
		operaciones.push(op);
	}

	@Override
	public ILista<VOOperacion> darHistoralOperacionesSR() {
		// TODO Auto-generated method stub


		return operaciones;
	}

	@Override
	public void limpiarHistorialOperacionesSR() {

		while(operaciones.darNumeroElementos()>0)
		{
			operaciones.pop();
		}

	}

	@Override
	public ILista<VOOperacion> darUltimasOperaciones(int n) {

		ListaEncadenada op = new ListaEncadenada<>();
		int i=0;
		while(i<n)
		{
			op.agregarElementoFinal(operaciones.pop());
			i++;
		}
		return op;
	}

	@Override
	public void borrarUltimasOperaciones(int n) {
		int i=0;
		while(i<n)
		{
			operaciones.pop();
			i++;
		}
	}

	//-----------------------------------------------------------------------
	//-------------------------------Parte C---------------------------------
	//-----------------------------------------------------------------------
	public void agregarOperacionSR1(String nomOperacion, long tinicio, long tfin) 
	{
		VOOperacion item = new VOOperacion();
		item.setOperacion(nomOperacion);
		item.setTimestampFin(tfin);
		item.setTimestampInicio(tinicio);
		pilaOperaciones.push(item);
	}


	public ILista<VOOperacion> darHistoralOperacionesSR1()
	{
		return pilaOperaciones;
	}


	public void limpiarHistorialOperacionesSR1() 
	{

		pilaOperaciones = new ListaEncadenada<>();

	}


	public ILista<VOOperacion> darUltimasOperaciones1(int n) 
	{


		ILista<VOOperacion> lista = new ListaEncadenada();
		for(int i=1; i<n;i++)
		{
			VOOperacion pop= (VOOperacion) pilaOperaciones.pop();
			lista.agregarElementoFinal(pop);

		}
		return lista;



	}


	public void borrarUltimasOperaciones1(int n) 
	{
		if(n<pilaOperaciones.darNumeroElementos())
		{
			for(int i=1; i<n;i++)
			{
				pilaOperaciones.pop();

			}
		}
		else
		{
			pilaOperaciones = new ListaEncadenada<>();
		}

	}

	public long agregarPelicula(String titulo, int agno, String[] generos) 
	{
		VOPelicula pelAgregar = new VOPelicula();
		pelAgregar.setTitulo(titulo);
		pelAgregar.setAgnoPublicacion(agno);
		ListaEncadenada<String> listaGeneros = new ListaEncadenada<String>();
		for(String genero:generos)
		{
			listaGeneros.agregarElementoFinal(genero);
		}
		pelAgregar.setGenerosAsociados(listaGeneros);
		pelAgregar.setIdUsuario(altoID()+1);
		peliculas.agregarElementoFinal((T) pelAgregar);
		return altoID()+1;

	}

	private long altoID()
	{
		long idMayor = Long.MIN_VALUE;

		Iterator<T> iter = peliculas.iterator();
		while(iter.hasNext())
		{
			VOPelicula actual = (VOPelicula) iter.next();
			if(actual.getIdpelicula()>idMayor)
				idMayor= actual.getIdpelicula();
		}
		return idMayor;
	}


	public void agregarRating(int idUsuario, int idPelicula, double rating)
	{

		VORating ratingAgregar = new VORating();
		ratingAgregar.setIdUsuario(idUsuario);
		ratingAgregar.setIdPelicula(idPelicula);
		ratingAgregar.setRating(rating);
		ratingAgregar.settimestamp(System.currentTimeMillis());

	}
}
