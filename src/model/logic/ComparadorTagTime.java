package model.logic;

import api.Comparator;
import model.vo.VOTag;

public class ComparadorTagTime implements Comparator<VOTag> {

	@Override
	public int compare(VOTag a, VOTag b) {
		if(a.getTimestamp()==b.getTimestamp())
			return 0;
			else if(a.getTimestamp()<b.getTimestamp())
			return 1;
			else if(a.getTimestamp()>b.getTimestamp())
			return -1;
		return 0;
	}

}
