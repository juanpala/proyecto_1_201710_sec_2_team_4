package model.logic;

import api.Comparator;
import model.vo.VOPelicula;

public  class ComparadorPeliculaID implements Comparator<VOPelicula>{

	
	public int compare(VOPelicula a, VOPelicula b) {
		// TODO Auto-generated method stub
		
		if(a.getIdpelicula()==b.getIdpelicula())
		return 0;
		else if(a.getIdpelicula()<b.getIdpelicula())
		return 1;
		else if(a.getIdpelicula()>b.getIdpelicula())
		return -1;
		
		return 0;
	}

	
}
