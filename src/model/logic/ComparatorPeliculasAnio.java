package model.logic;

import api.Comparator;

import model.vo.VOPelicula;

public class ComparatorPeliculasAnio implements Comparator<VOPelicula>{

	@Override
	public int compare(VOPelicula o1, VOPelicula o2)
	{
		if(o1.getAgnoPublicacion()>o2.getAgnoPublicacion())
			return 1;
		else if(o1.getAgnoPublicacion()==o2.getAgnoPublicacion())
			return 0;
		else
			return -1;
	}

}
