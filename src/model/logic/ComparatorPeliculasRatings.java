package model.logic;

import api.Comparator;

import model.vo.VOPelicula;

public class ComparatorPeliculasRatings implements Comparator<VOPelicula> 
{

	public int compare(VOPelicula pel1, VOPelicula pel2)
	{
		if(pel1.getPromedioRatings()> pel2.getPromedioRatings())
			return 1;
		else if(pel1.getPromedioRatings()== pel2.getPromedioRatings())
		return 0;
		else
			return -1;
	}

}
