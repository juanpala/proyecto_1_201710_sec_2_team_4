package model.data_structures;


public class NodoDoble<T> {
	
	// Representa al nodo inmediatamente anterior al actual
	private NodoDoble<T> anterior;

	//Representa el nodo siguiente al actual.
	private NodoDoble<T> siguiente;

	//Entidad gen�rica que da valores al nodo actual de la lista. 
	private T elementoActual;
	
	public NodoDoble(T elemtentoAgregar)
	{
		elementoActual =elemtentoAgregar;
		anterior = null;
		siguiente = null;

	}

	public NodoDoble<T> getAnterior() {
		return anterior;
	}

	public void setAnterior(NodoDoble<T> anterior) {
		this.anterior = anterior;
	}

	public NodoDoble<T> getSiguiente() {
		return siguiente;
	}

	public void setSiguiente(NodoDoble<T> siguiente) {
		this.siguiente = siguiente;
	}

	public T getElementoActual() {
		return elementoActual;
	}

	public void setElementoActual(T elementoActual) {
		this.elementoActual = elementoActual;
	}
}
