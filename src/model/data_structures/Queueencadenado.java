package model.data_structures;

import api.IQueue;

public class Queueencadenado<T> implements IQueue<T>{

    NodoSencillo<T> cabeza ;
    NodoSencillo<T> fin ;
  
    public Queueencadenado()
    {
    	Queue();
    }
	public void Queue() {
		
		cabeza = null;
		fin = null;
		
	}

	@Override
	public void enqueue(T item) {
		if(cabeza==null)
		{
			cabeza = new NodoSencillo<T>(item);
			
		}
		else
		{
			boolean agrego = false;
			NodoSencillo<T> m = cabeza;
			while(!agrego)
			{
				if(m.getSiguiente()==null)
				{
					m.setSiguiente(new NodoSencillo<T>(item));
					fin = m.getSiguiente();
					agrego = true;
				}
				else
				{
					m = m.getSiguiente();
				}
			}
		}
		
	}

	@Override
	public T dequeue() {
		T elem = null;
		if(cabeza==null)
		{
			return elem;
		}
		else
		{
			elem = cabeza.getNodo();
			cabeza = cabeza.getSiguiente();
		}
		return elem;
	}

	@Override
	public boolean isEmpty() {
		if(cabeza==null)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

}
