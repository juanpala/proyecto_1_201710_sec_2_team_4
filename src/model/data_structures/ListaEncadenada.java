package model.data_structures;

import java.util.Iterator;

import api.Comparator;
import api.Istacks;
import model.data_structures.ListaDobleEncadenada.ListIterator;


public class ListaEncadenada<T extends Comparable<T>>implements ILista<T>,Istacks<T>  {


	private NodoSencillo<T> cabeza ;
	int tama�o ;
	private Iterator<T> it = iterator() ;

	private class ListIterator implements Iterator<T>
	{
		private NodoSencillo current;
		public ListIterator()
		{
			current = cabeza;
		}

		public boolean hasNext() { return current !=cabeza ; }
		public void remove() { /* not supported */ }
		public T next()
		{
			NodoSencillo<T> nod = current;
			current = current.getSiguiente();

			return nod.getNodo();
		}
		public NodoSencillo getcurrent()
		{
			return current;
		}
		public NodoSencillo no()
		{
			return current;
		}
	} 

	public Iterator<T> iterator() {

		return new ListIterator();
	}


	public void agregarElementoFinal(T elem,Comparator<T> comparador) {
		
		NodoSencillo<T> nodo = cabeza;
	
		if(cabeza == null)
		{
			cabeza = new NodoSencillo<T>(elem);
			tama�o++;
			it = iterator();	
		}
		else
		{
			boolean elementoAgregado = false;
			while(!elementoAgregado)
			{
			
				if(comparador.compare(nodo.getNodo(), elem)==0)
				{
					if(nodo.getSiguiente()==null)
					{
					  nodo.setSiguiente(new NodoSencillo(elem));
					  tama�o++;
						elementoAgregado = true;
					}
					else
					{
						if(	comparador.compare(nodo.getNodo(), elem)<0)
						{
							NodoSencillo<T> ne = new NodoSencillo(elem);
							ne.setSiguiente(nodo.getSiguiente());
							nodo.setSiguiente(ne);
							tama�o++;
							elementoAgregado = true;
						}
						else
						{
							nodo = nodo.getSiguiente();
						}
					}
				}
			  if(nodo.getSiguiente()==null)
			  {
				  nodo.setSiguiente(new NodoSencillo(elem));
				  tama�o++;
					elementoAgregado = true; 
			  }
			  else
			  {
				 if(comparador.compare(nodo.getSiguiente().getNodo(), elem)<0)
				{
				    
				    
				    		NodoSencillo<T> ne = new NodoSencillo(elem);
							ne.setSiguiente(nodo.getSiguiente());
							nodo.setSiguiente(ne);
							tama�o++;
							elementoAgregado = true;
				  
				}
				else if(comparador.compare(nodo.getSiguiente().getNodo(), elem)>=0)
				{
				
					
						nodo = nodo.getSiguiente();
					
		        }
			}
			}
		}
//		{
//			boolean agrego = false;
//			while(!agrego)
//			{
//				NodoSencillo<T> cos =((ListIterator) it).no();
//				if(cos.getSiguiente()==null)
//				{
//					NodoSencillo<T> nuevo = new NodoSencillo<T>(elem);
//					cos.setSiguiente(nuevo);
//					tama�o ++;
//					agrego = true;
//				}
//				else
//				{
//					it.next();
//				}
//			}	  
//		}	
}

	public void agregarElementopos(int pos,T elem)
	{ 
		
	    int i = 0;
	    NodoSencillo<T> nodo = this.cabeza;
		if(cabeza==null)
		{
			cabeza = new NodoSencillo<T>(elem);
			tama�o++;
		}
		else
		{

			boolean agrego = false;
			while(i<pos&&!agrego)
			{
				
				NodoSencillo<T> elementoActual = nodo.getSiguiente();
	
				if(pos-1==i)
				{

				
					elementoActual.setelementoActual(elem);
			
					agrego =true;
				

					NodoSencillo<T> nuevo = new NodoSencillo<T>(elem);
					
					elementoActual.setSiguiente(nuevo);
					tama�o ++;
					agrego = true;

				}
				else
				{
					i++;
					nodo= elementoActual;
	
				}
				}
			}
		
	}



	public T darElemento(int pos)
	{

		int rmovi = 0;
		T elemento = null;

		NodoSencillo<T> nod = cabeza;
		if(pos == 0)
		{
			elemento = nod.getNodo();
		}
		else
		{
			while(rmovi<pos)
			{
				nod = nod.getSiguiente();
				elemento = nod.getNodo();
				rmovi ++;

			}
		}
		return elemento;
	}

	public NodoSencillo<T> darNodoPos(int pos)
	{
		int rmovi = 0;


		NodoSencillo<T> nod = cabeza;
		if(pos == 0)
		{
			nod = cabeza;
		}
		if(pos!=0)
		{
			while(rmovi<pos)
			{
				nod = nod.getSiguiente();
				rmovi ++;

			}
		
		}
		
		return nod;
	}

	public T eliminarElemento(int pos) {
		int rmovi = 1;
		T elemento = null;

		NodoSencillo<T> nod = cabeza;
		if(pos == 1)
		{
			elemento = cabeza.getNodo();
			cabeza = cabeza.getSiguiente();
		}
		else
		{
			boolean encontro = false;
			while(!encontro)
			{

				if(rmovi == pos-1)
				{
					elemento = nod.getSiguiente().getNodo();
					NodoSencillo<T> nor = nod.getSiguiente().getSiguiente();
					if(nor==null)
					{
						nod.setSiguiente(null);
					}
					else
					{
						nod.setSiguiente(nor);
					}

					encontro = true;
				}
				else
				{
					rmovi ++;
				}
			}
		}
		return elemento;
	}


	public int darNumeroElementos() {
		// TODO Auto-generated method stub
		return tama�o;
	}


	public void Stack() {
		cabeza = null;
		tama�o = 0;

	}


	public void push(T item) {

		if(!isEmpty())
		{
			cabeza=new NodoSencillo<T>(item);
			tama�o ++;
		}
		else
		{
			NodoSencillo<T> nod = new NodoSencillo<T>(item);
			nod.setSiguiente(cabeza);
			cabeza = nod;
			tama�o++;
		}

	}


	public T pop() {
		if(darNumeroElementos()==0)
		{
			return null;
		}
		else
		{
			NodoSencillo<T> pop = cabeza;
			cabeza = cabeza.getSiguiente();
			tama�o--;
			return pop.getNodo();
		}

	}


	public boolean isEmpty() {
		if(size()==0)
		{
			return true;
		}
		else
		{
			return false;
		}

	}


	public int size() {
		// TODO Auto-generated method stub
		return tama�o;
	}

	@Override
	public ListaEncadenada<T> partirPorLaMitad()
	{
		int mitad = tama�o / 2 - 1;
		ListaEncadenada<T> segundaMitad = new ListaEncadenada<>();
		for(int elemento = mitad; elemento<tama�o; elemento++)
		{
			NodoSencillo<T> mitadLista = darNodoPos(elemento);
			T elem =mitadLista.getNodo();
			segundaMitad.agregarElementoFinal(elem);
		}
		return segundaMitad;
	}
	//	public ListaEncadenada<T> partirPorLaMitad()
	//	{
	//		int mitad = tamanioLista / 2 - 1;
	//		NodoEncadenado<T> mitadLista = darNodoPosicion(mitad);
	//		ListaEncadenada<T> segundaMitad = new ListaEncadenada<>(mitadLista.darSiguiente(), ultimo, tamanioLista-mitad-1);
	//		tamanioLista = mitad +1;
	//		if(posicionActual > mitad)
	//		{
	//			posicionActual = mitad;
	//		}
	//		ultimo = mitadLista;
	//		ultimo.cambiarSiguiente(null);
	//		return segundaMitad;
	//	}

	public void agregarAlPrincipio(T item)
	{
	NodoSencillo<T>	itemAgregar = new NodoSencillo<T>(item);
	if(tama�o==0)
	{
		cabeza = itemAgregar;
	}
	}
	@Override
	public void agregarElementoFinal(T elem) {
		NodoSencillo<T> nodo = cabeza;
		if(cabeza == null)
		{
			cabeza = new NodoSencillo<T>(elem);
			tama�o++;
			it = iterator();	
		}
		else
		{
			boolean agrego = false;
			while(!agrego)
			{
	        NodoSencillo<T> sig = nodo.getSiguiente();
	        if(sig==null)
	        {
	        	nodo.setSiguiente(new NodoSencillo(elem));
	        	tama�o++;
	        	agrego = true;

			}
	        else
	        {
	        	nodo=sig;
	        }
	   
		}
	}
	}
	



	public T eliminarPrimerElemento()
	{
		
		if(tama�o>0)
		{
			NodoSencillo<T> eliminar = cabeza;
			eliminar.setSiguiente(null);
		cabeza= cabeza.getSiguiente();
		tama�o--;
			return eliminar.getNodo();
		}
		else
		{
		return null;
		}
	
		
	}
}
