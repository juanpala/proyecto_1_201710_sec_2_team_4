package model.data_structures;

import java.util.ArrayList;

import api.Comparator;
import model.vo.VOUsuario;

public class QuickSort <T extends Comparable<T>>{
	
	private static ArrayList ss;
	
//	private static int partition(ArrayList a, int lo,int hi,Comparator compa)
//	{
////		int i = lo, j = hi+1; 
////          
////	
////         while(true)
////         {
////         	 while((( (Comparable) a.get(++i)).compareTo(a.get(lo))<0))
////        	 {
////        		 if(i==hi)
////        			 
////        			 break;
////        	 }
////        	 while(((Comparable) a.get(lo)).compareTo(a.get(--j))<0)
////        	 {
////        		 if(j==lo)
////        			 
////        			 break;
////        	 }
////        	 if(i>=j) break;
////        	 cambiar(a,i,j);
////         }
////         cambiar(a,lo,j);
////         return j;
//}

	public static void sort(ListaEncadenada tags,Comparator comp)
	{
		 ss = new ArrayList();
		 if(tags==null || tags.darNumeroElementos()==0)
		 {
			 return;
		 }
         for(int i=0;i<tags.darNumeroElementos()-1;i++)
         {
        	 ss.add(tags.darElemento(i));
         }
    	sort(ss,0,ss.size()-1, comp);
    	tags = new ListaEncadenada<>();
        for(int i=0;i<=ss.size()-1;i++)
        {
       	 tags.agregarElementoFinal((Comparable) ss.get(i));
        }
	}
	private static void sort(ArrayList tag, int lo, int hi,Comparator compa) {
	 
	int i = lo,j=hi;
	
	Comparable u = (Comparable) tag.get(lo+hi/2);
	
	while (i<=j)
	{
		while(compa.compare(tag.get(i), u)<0){
			i++;
		}
		while(compa.compare(tag.get(j), u)<0){
			j--;
		}
	   if(i<=j)
	   {
		   cambiar(tag, i, j);
		   i++;
		   j--;
	   }
	   if(lo<j)
		   sort(tag,lo,j,compa);
	   if(i<hi)
		   sort(tag,i,hi,compa);
	}
//	  if(hi<=lo) return;
//	  int j = partition(a, lo, hi,compa);
//	  sort(tag,a,lo,j-1, compa);
//	  sort(tag,a,j+1,hi,compa);
//	  for(int i = 0;i<a.size()-1;i++ )
//	  {
//		  tag.agregarElementoFinal((Comparable) a.get(i));
//		  VOUsuario hj = (VOUsuario) a.get(i);
//		  System.out.println(hj.getIdUsuario()+"   " +hj.getPrimerTimestamp());
//	  }
	}

	public static void cambiar (ArrayList tag,int c,int b)
	{
	   Comparable o = (Comparable) tag.get(b);
	 tag.set(b, tag.get(c));
	  tag.set(c, o);;
	

	}
}
