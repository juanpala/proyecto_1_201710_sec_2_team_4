package model.data_structures;


	public class NodoSencillo<T> {
	private NodoSencillo<T> Siguiente ;
	private T elemento ;
	public NodoSencillo(T elem){
		elemento = elem;
		Siguiente = null;
	}

	public T getNodo()
	{
		return elemento;
	}
	
	public NodoSencillo<T> getSiguiente() {
		return Siguiente;
	}

	public void setSiguiente(NodoSencillo<T> siguiente) {
		Siguiente = siguiente;
	}
	public void setelementoActual(T elem)
	{
		elemento = elem;
	}

}
