package model.data_structures;

import java.util.Comparator;
import java.util.Iterator;

/**
 * Clase que generar� el ordenamidento mediante merge.
 * @author Christian Chavarro, Sebastian Palacios
 */
public class MergeSort<T> {



	private static void merge(ArregloFlexible a, ArregloFlexible aux, int lo, int mid, int hi)
	{
		for (int k = lo; k <= hi; k++) 
		{
			aux.agregarElementoPos(k, a.darElemento(k));
		}

		int i = lo, j = mid + 1;

		for (int k = lo; k <= hi; k++)
		{
			if (i > mid)
			{

				a.agregarElementoPos(k, aux.darElemento(j++));
			} else if (j > hi)
			{

				a.agregarElementoPos(k, aux.darElemento(i++));
			} 
			else if (aux.darElemento(j).compareTo(aux.darElemento(i)) < 0)
			{

				a.agregarElementoPos(k, aux.darElemento(j++));

			}
			else {

				a.agregarElementoPos(k, aux.darElemento(i++));



			}

		}
	}

	public static void sort(ArregloFlexible a, ArregloFlexible aux, int lo, int hi) {
		if (hi <= lo)
			return;
		int mid = lo + (hi - lo) / 2;
		sort(a, aux, lo, mid);
		sort(a, aux, mid + 1, hi);
		merge(a, aux, lo, mid, hi);
	}

	public static void sort(ArregloFlexible a) {
		ArregloFlexible aux = new ArregloFlexible(a.darTamanoAct());
		sort(a, aux, 0, a.darTamanoAct() - 1);
	}

	private static <T extends Comparable<T>> void merge(ListaDobleEncadenada<T> a, ListaDobleEncadenada<T> aux,
			int lo, int mid, int hi) {
		for (int k = lo; k <= hi; k++) 
		{
			aux.agregarelemento(k, a.darElemento(k));
		}

		int i = lo, j = mid + 1;

		for (int k = lo; k <= hi; k++) {
			if (i > mid) {
				a.agregarelemento(k,aux.darElemento(j++));
			} else if (j > hi) {
				a.agregarelemento(k,aux.darElemento(i++));
			} else if (aux.darElemento(j).compareTo(aux.darElemento(i)) < 0) {
				a.agregarelemento(k,aux.darElemento(j++));
			} else if (aux.darElemento(j).compareTo(aux.darElemento(i)) > 0) {
				a.agregarelemento(k, aux.darElemento(i++));
			}
		}
	}

	public static <T extends Comparable<T>> void sort(ListaDobleEncadenada<T> a, ListaDobleEncadenada<T> aux, int lo,
			int hi) {
		if (hi <= lo)
			return;
		int mi = lo + (hi - lo) / 2;
		sort(a, aux, lo, mi);
		sort(a, aux, mi + 1, hi);
		merge(a, aux, lo, mi, hi);
	}

	public static <T extends Comparable<T>> void sort(ListaDobleEncadenada<T> a) {
		ListaDobleEncadenada<T> aux = new ListaDobleEncadenada<T>();
		sort(a, aux, 0, a.darNumeroElementos() - 1);
	}

	public static <T> ILista<T> sort(ILista<T> listaOrdenar, Comparator<T> comparador) throws InstantiationException, IllegalAccessException
	{
		if(listaOrdenar.darNumeroElementos() <= 1)
		{
			return listaOrdenar;
		}
		ILista<T> segundaMitad = listaOrdenar.partirPorLaMitad();
		ILista<T> primeraMitadOrdenada = sort(listaOrdenar, comparador);
		ILista<T> segundaMitadOrdenada = sort(segundaMitad, comparador);
		@SuppressWarnings("unchecked")
		ILista<T> listaAuxiliar = listaOrdenar.getClass().newInstance();
		Iterator<T> iteradorPrimeraMitad = primeraMitadOrdenada.iterator();
		Iterator<T> iteradorSegundaMitad = segundaMitadOrdenada.iterator();
		T elementoActualPrimeraMitad = null;
		T elementoActualSegundaMitad = null;
		while(iteradorPrimeraMitad.hasNext()|| iteradorSegundaMitad.hasNext())
		{
			if(elementoActualPrimeraMitad == null && iteradorPrimeraMitad.hasNext())
			{elementoActualPrimeraMitad = iteradorPrimeraMitad.next();}

			if(elementoActualSegundaMitad == null && iteradorSegundaMitad.hasNext())
			{elementoActualSegundaMitad = iteradorSegundaMitad.next();}


			if(elementoActualSegundaMitad == null || (elementoActualPrimeraMitad !=null && comparador.compare(elementoActualPrimeraMitad, elementoActualSegundaMitad) <= 0))
			{
				listaAuxiliar.agregarElementoFinal(elementoActualPrimeraMitad);
				elementoActualPrimeraMitad = null;
			}
			else
			{
				listaAuxiliar.agregarElementoFinal(elementoActualSegundaMitad);
				elementoActualSegundaMitad = null;
			}			
		}

		if(elementoActualPrimeraMitad != null)
		{	listaAuxiliar.agregarElementoFinal(elementoActualPrimeraMitad);}

		if(elementoActualSegundaMitad != null)
		{listaAuxiliar.agregarElementoFinal(elementoActualSegundaMitad);}

		return listaAuxiliar;

	}
}
