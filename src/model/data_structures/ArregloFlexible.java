package model.data_structures;

public class ArregloFlexible<T extends Comparable<T>>{
	private T[] elementos;
	private int tamanoMax ;
	private int tamanoAct;

	 public ArregloFlexible( int max )
	 {
	 elementos = (T []) new Comparable[max];
	 tamanoMax = max;
	 tamanoAct = 0;
	 }

	 public void agregarElem( T dato )
	 {
	  if ( tamanoAct == tamanoMax )
	  { 
	  tamanoMax = 2 * tamanoMax;
	  T [ ] copia = elementos;
	  elementos =  (T[]) new Comparable[tamanoMax];
	  for ( int i = 0; i < tamanoAct; i++)
	  {
	  elementos[i] = copia[i];
	 }
	  }
	  elementos[tamanoAct] = dato;
	  tamanoAct++;
	 }
	 public void agregarElementoPos(int i,T dato)
	 {
		 if(i<darTamanoAct())
		 {
		 elementos[i]= dato;
		 }
		 else
		 {
			 agregarElem(dato);
		 }
	 }
	 public int darTamanoAct( )
	 {
	  return tamanoAct;
	 }
	 public T darElemento( int i )
	 {
	  return elementos[i];
	 }
	 T [ ] darElementos( )
	 {
	  return elementos;
	 }

	
	

}
