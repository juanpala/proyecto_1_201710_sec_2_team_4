package model.data_structures;

import java.util.Iterator;

import api.Comparator;
import api.Istacks;

import model.data_structures.ListaDobleEncadenada.ListIterator;import model.logic.ComparadorPeliculaID;

import model.data_structures.ListaDobleEncadenada.ListIterator;
import model.vo.VOPelicula;


public class ListaDobleEncadenada<T extends Comparable<T>>implements ILista<T> {

	// Atributos
	//Representa la cantidad de elementos presentes en la lista
	private int cantidadElementos; 

	//Mantiene una referencia al nodo inicial de la lista.
	private NodoDoble<T> nodoInicial;

	//Referencia el elemento actualmente seleccionado
	private T elementoActual;

	//El iterador de la lista
	private Iterator<T> iter;
	
	
	public class  ListIterator implements Iterator<T> 
	{
		//Atributos
		//Representa al nodo actual sobre el cual est� pasando el iterador.

		private NodoDoble  current ;

		/**
		 * M�todo constructor de la clase, da valor al current con respecto al atributo nodoInicial de la clase lider
		 */
		public ListIterator()
		{
			current =  nodoInicial;
		}

		/**
		 * M�todo boleano que indica si hay un siguiente elemento en la lista actual
		 */
		public boolean hasNext() 
		{
			return current.getSiguiente()!=null;
		}
		
		/**
		 * M�todo que retorna el valor asignado al objeto, si existe este.
		 */
		public T next() 
		{
			NodoDoble<T> nodo = current;
			current = current.getSiguiente();
			return nodo.getElementoActual();
		}
		
		/**
		 * M�todo remove, no se implementa en esta estructura de datos.
		 */
		public void remove()
		{
			//not suported.
		}		

		/**
		 * M�todo que retorna el valor del current
		 * @return current.
		 */
		public NodoDoble<T> getCurrent()
		{
			return current;
		}
	}
	
	public Iterator<T> iterator() {
		// TODO Auto-generated method stub
		return  new ListIterator();
	}

	public void agregarElementoFinal(T elem,Comparator<T> comparador) {
		if (nodoInicial ==null)
		{
			nodoInicial = new NodoDoble<T>(elem);
			cantidadElementos++;
			iter = iterator();
		}
		else
		{
			boolean elementoAgregado = false;
			while(!elementoAgregado)
			{
				
				NodoDoble<T> nodo = ((ListIterator) iter).getCurrent(); 
				if(comparador.compare(nodo.getElementoActual(), elem)==0)
				{
					if(nodo.getSiguiente()==null)
					{
					  nodo.setSiguiente(new NodoDoble(elem));
					}
					else
					{
						if(	comparador.compare(nodo.getElementoActual(), elem)<0)
						{
							NodoDoble<T> ne = new NodoDoble(elem);
							ne.setAnterior(nodo);
							ne.setSiguiente(nodo.getSiguiente());
							nodo.getSiguiente().getAnterior().setAnterior(ne);
							nodo.setSiguiente(ne);
							cantidadElementos++;
							elementoAgregado = true;
						}
						else
						{
							iter.next();
						}
					}
				}
			
				else if(comparador.compare(nodo.getElementoActual(), elem)<0)
				{
				     if(nodo.getAnterior()==null)
				     {
				    
						NodoDoble<T> ne = new NodoDoble(elem);
						ne.setSiguiente(nodo);
						nodo.setAnterior(ne);
						cantidadElementos++;
						elementoAgregado = true;
						 
				     }
				     else 
				     {
				    	 NodoDoble<T> ne = new NodoDoble(elem);
				    	 ne.setSiguiente(nodo);
				    	 ne.setAnterior(nodo.getAnterior());
				    	 nodo.getAnterior().setSiguiente(ne);
				    	 nodo.setAnterior(ne);
				    	 cantidadElementos++;
				    	 elementoAgregado = true;
				     }
					
				}
				else if(comparador.compare(nodo.getElementoActual(), elem)>0)
				{
					if(nodo.getSiguiente()==null)
					{
						 NodoDoble<T> ne = new NodoDoble(elem);
						 ne.setAnterior(nodo);
						 nodo.setSiguiente(ne);
						 elementoAgregado = true;
						 cantidadElementos++;
					}
					else
					{
						iter.next();
					}
				
				}
//				if(nodo.getSiguiente()== null)
//				{
//					NodoDoble<T> agregar = new NodoDoble<T>(elem);
//					nodo.setSiguiente(agregar);
//					agregar.setAnterior(nodo);
//					cantidadElementos++;
//					elementoAgregado = true;
//				}
//				else
//				{
//					iter.next();
//				}
			}
		}
		
	}

	@Override
	public T darElemento(int pos) {

		int i =0;
		T elemento = null;
		NodoDoble<T> nodo = nodoInicial;
		if(pos == 0)
		{
			elemento= nodo.getElementoActual();
		
		}
		else
		{
			while(i<pos)
			{
				NodoDoble<T> elementoActual = nodo.getSiguiente();
				elemento = elementoActual.getElementoActual();
				i++;
				nodo= elementoActual;
				
			}

		}
		return elemento;
	}

	@Override
	public T eliminarElemento(int pos) {

        int i = 0;
        T elemento = null;
        NodoDoble<T> nodo = nodoInicial;
        if(pos==0)
        {
        	elemento = nodo.getElementoActual();
        	nodoInicial = nodo.getSiguiente();
        	cantidadElementos--;
        }
        else
        {
        	boolean elemino = false;
        	while(!elemino)
        	{
        		NodoDoble<T> elementoActual = nodo.getSiguiente();
        		NodoDoble<T> anterior = elementoActual.getAnterior();
        		if(i==pos)
        		{
        			if(elementoActual.getSiguiente()!=null)
        			{
        			
        			anterior.setSiguiente(elementoActual.getSiguiente());
        			cantidadElementos--;
        			elemino =true;
        			}
        			else 
        			{
        				anterior.setSiguiente(null);
        				cantidadElementos--;
        				elemino =true;
        			}
        		}
        		else
        		{
        			elementoActual = elementoActual.getSiguiente();
    				i++;
    				
        		}

        	}
        }
        return elemento;
        
	}

	@Override
	public int darNumeroElementos() {
		// TODO Auto-generated method stub
		return cantidadElementos;
	}


	public void agregarElementoFinal(T elem) {
		// TODO Auto-generated method stub
		
	}


		public void agregarelemento(int pos,T elem)
	{
		int i = 0;
		NodoDoble<T> nodo = this.nodoInicial;
		if(pos == 0)
		{
			if(nodo==null)
			{
				agregarElementoFinal(elem);
			}
			else
			{
			nodoInicial.setElementoActual(elem);
			}
		}
		else
		{
			boolean agrego = false;
			while(i<pos&&!agrego)
			{
				
				NodoDoble<T> elementoActual = nodo.getSiguiente();
				if(elementoActual==null)
				{
					agregarElementoFinal(elem);
				}
				else
				{
				if(pos-1==i)
				{
				
					elementoActual.setElementoActual(elem);
			
					agrego =true;
				}
				else
				{
					i++;
					nodo= elementoActual;
	
				}
							}
			}
		}
	}

//		int mitad = numeroDeNodos / 2 - 1;
//		NodoDoblementeEncadenado<T> mitadLista = darNodoPosicion(mitad);
//		ListaDobleEncadenada<T> segundaMitad = new ListaDobleEncadenada<>(mitadLista.darSiguiente(), ultimo, numeroDeNodos-mitad-1);
//		numeroDeNodos = mitad +1;
//		if(nodoActual > mitad)
//		{
//			nodoActual = mitad;
//		}
//		ultimo = mitadLista;
//		ultimo.cambiarSiguiente(null);
//		return segundaMitad;
		
//		int mitad = tama�o / 2 - 1;
//		ListaEncadenada<T> segundaMitad = new ListaEncadenada<>();
//		for(int elemento = mitad; elemento<tama�o; elemento++)
//		{
//			NodoSencillo<T> mitadLista = darNodoPos(elemento);
//			T elem =mitadLista.getNodo();
//			segundaMitad.agregarElementoFinal(elem);
//		}
//		return segundaMitad;

		public ILista<T> partirPorLaMitad() 
		{
		
		  int mitad = (cantidadElementos/2)-1;
		  //NodoDoble<T> nodoMitad = darElemento(mitad);
		  return null;
		}


}
