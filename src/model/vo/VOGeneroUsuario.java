package model.vo;

import model.data_structures.ILista;

public class VOGeneroUsuario implements Comparable<VOGeneroUsuario> {
	
	private String genero;
	private ILista<VOUsuarioConteo> usuarios;
	public String getGenero() {
		return genero;
	}
	public void setGenero(String genero) {
		this.genero = genero;
	}
	public ILista<VOUsuarioConteo> getUsuarios() {
		return usuarios;
	}
	public void setUsuarios(ILista<VOUsuarioConteo> usuarios) {
		this.usuarios = usuarios;
	}
	@Override

	public String toString()
	{
		String usu = "";
		return "< "+ genero+" >" +"[" +usu+"]";
	}
	@Override
	public int compareTo(VOGeneroUsuario o) {
		// TODO Auto-generated method stub
		return 0;
	}

}
