package model.vo;

import model.data_structures.ILista;

public class VOGeneroPelicula implements Comparable<VOGeneroPelicula> {
	
	private String genero;
	
	private ILista<VOPelicula> peliculas;

	public String getGenero() {
		return genero;
	}

	public void setGenero(String genero) {
		this.genero = genero;
	}

	public ILista<VOPelicula> getPeliculas() {
		return peliculas;
	}

	public void setPeliculas(ILista<VOPelicula> peliculas) {
		this.peliculas = peliculas;
	}

	@Override
	public int compareTo(VOGeneroPelicula o) {
		// TODO Auto-generated method stub
		return 0;
	} 
	public String toString()
	{
		String pel="";
		for(int i =0;i<peliculas.darNumeroElementos();i++)
		{
			pel= pel + peliculas.darElemento(i).getTitulo();
		}
		return genero +"  "+pel;
	}

}
