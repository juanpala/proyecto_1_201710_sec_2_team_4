package model.vo;

public class VOTag implements Comparable<VOTag> {
	private String tag;
	private long timestamp;
	private long userid;
	public long getUserid() {
		return userid;
	}
	public void setUserid(long userid) {
		this.userid = userid;
	}
	public String getTag() {
		return tag;
	}
	public void setTag(String tag) {
		this.tag = tag;
	}
	public long getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(long timestamp) {
		this.timestamp = timestamp;
	}
	@Override
	public int compareTo(VOTag o) {
		
		if(this.timestamp==o.getTimestamp())
		{
			return 0;
		}
		else if(this.timestamp<o.getTimestamp())
		{
			return -1;
		}
		else if(this.timestamp>o.getTimestamp())
		{
			return 1;
		}
		
		return 0;
	}
	public String toString()
	{
		return tag + timestamp;
	}
	public int compareTo(String o) 
   {
		if(this.tag.compareTo(o)==0)
		{
			return 0;
		}
		else if(this.tag.compareTo(o)<0)
		{
			return -1;
		}
		else if(this.tag.compareTo(o)>0)
		{
			return 1;
		}
		
			return 0;
	}
	}
	

