package view;

import java.util.Scanner;

import controller.Controller;

public class VistamManejadorSR
{

	public static void main(String[] args) {

		Scanner sc=new Scanner(System.in);
		boolean fin=false;
		while(!fin)
		{
			printMenu();

			int option = sc.nextInt();
			sc.nextLine();

			switch(option)
			{		
			case 1:	
	
				Controller.cargarPeliculasSR("data/movies.csv");
				//TODO el m�todo cargar ratings, no funciona, se deja como comentario la implementacion del mismo.
				Controller.cargarRatingsSR("data/ratings.csv");
				Controller.cargarTagsSR("data/tags.csv");
				Controller.cargarRatingsSR("Data/ratings.csv");
				System.out.println("Se cargo el sistema de recomendaci�n con exito.");
				break;

			case 2:
				System.out.println(Controller.sizeMoviesSR());
				break;

			case 3: 
				System.out.println(Controller.sizeUsersSR());
				break;
			case 4:
				System.out.println(Controller.sizeTagsSR());
				break;
			case 5:
				System.out.println("Ingrese el n�mero de peliculas que desea");
				int numPeliculas = sc.nextInt();
				System.out.println(Controller.peliculasPopularesSR(numPeliculas));
				break;
			case 6:
				System.out.println(Controller.catalogoPeliculasOrdenadoSR());
				break;
			case 7:
				System.out.println(Controller.recomendarGeneroSR());
				break;
			case 8:
				System.out.println(Controller.opinionRatingsGeneroSR());
				break;
			case 9:
				System.out.println("Ingrese el n�mero de peliculas que quiere que se considere");
				int numPeliculas2 = sc.nextInt();
				//TODO agregar la ruta de recomendaci�n de las peliculas	
				System.out.println(Controller.recomendarPeliculasSR("", numPeliculas2));
				break;
			case 10:
				System.out.println("Ingrese un id de pelicula");
				long idPelicula = sc.nextLong();
				System.out.println(Controller.ratingsPeliculaSR(idPelicula));
				break;
			case 11:
				System.out.println("Ingrese el n�mero de usuarios que desea");
				int numUsuarios = sc.nextInt();
				System.out.println(Controller.usuariosActivosSR(numUsuarios));
				break;
			case 12:
				System.out.println(Controller.catalogoUsuariosOrdenadoSR());
				break;
			case 13:
				System.out.println("Ingrese la cantidad de peliculas a buscar:");
				int numPeliculas3 = sc.nextInt();
				System.out.println(Controller.recomendarTagsGeneroSR(numPeliculas3));
				break;
			case 14:
				System.out.println(Controller.opinionTagsGeneroSR());
				break;
			case 15:
				System.out.println("Ingrese el n�mero a buscar");
				int numPeliculas4 = sc.nextInt();
			//TODO agregar la ruta de recomendaci�n de los usuarios	
			System.out.println(Controller.recomendarUsuariosSR("", numPeliculas4));
				break;
			case 16: 
				System.out.println("Ingrese un id para pelicula");
				int idPelicula2 = sc.nextInt();
				System.out.println(Controller.tagsPeliculaSR(idPelicula2));
				break;
			case 17:
				System.out.println(Controller.darHistoralOperacionesSR());
				break;
			case 18:
				Controller.limpiarHistorialOperacionesSR();
				System.out.println("Se borro el historial");
				break;
			case 19:
				System.out.println("Ingrese el n�mero de operaciones:");
				int numOperaciones = sc.nextInt();
				System.out.println(Controller.darUltimasOperaciones(numOperaciones));
				break;
			case 20:
				System.out.println("Ingrese el n�mero de operaciones a eliminar:");
				int numOperaciones2 = sc.nextInt();
				Controller.borrarUltimasOperaciones(numOperaciones2);
				if(numOperaciones2 == 1)
				{
					System.out.println("Se elimino 1 operaci�n.");
				}
				else
				{
					System.out.println("Se eliminaron "+numOperaciones2+" operaciones.");
				}
				break;
			case 21:
				System.out.println("Ingrese el titulo, a�o y generos asociados. Separados por coma. Si tiene m�s de un g�nero, estos deben ir separados por punto y coma.(ejem: Ch, 2019, Amor;Soledad");
				String linea = sc.nextLine();		
				String[] partesPelicula = linea.split(",");
				String[] genero = partesPelicula[2].split(";");
				try{
					Controller.agregarPelicula(partesPelicula[0].trim(), Integer.parseInt(partesPelicula[1].trim()), genero);
				}
				catch(NumberFormatException e)
				{
					System.out.println("Alguno de los datos esta mal.");
					printMenu();
				}		
				break;
			case 22:
				System.out.println("Ingrese el id del usuario, id de pelicula y rating. Separados por coma. \n(ejem: 101, 1, 5.0");
				String[] partesRating = sc.nextLine().split(",");				
				try{
					Controller.agregarRating(Integer.parseInt(partesRating[0]), Integer.parseInt(partesRating[1]), Double.parseDouble(partesRating[2]));
				}
				catch(NumberFormatException e)
				{
					System.out.println("Alguno de los datos esta mal.");
					printMenu();
				}	
				break;
			case 23:
				fin = true;
				
				break;
			}
		}
		sc.close();
	}

	private static void printMenu() 
	{
		System.out.println("---------ISIS 1206 - Estructuras de datos----------");
		System.out.println("---------------------Proyecto 1----------------------");
		System.out.println("---------------------Team 4----------------------");
		System.out.println("1. Cargar sistema de recomendaci�n de peliculas");
		System.out.println("2. Dar el n�mero de peliculas en el SR");
		System.out.println("3. Dar el n�mero de usuarios en el SR");
		System.out.println("4. Dar el n�mero de tags en el SR");
		System.out.println("5. Lista peliculas m�s populares del SR");
		System.out.println("6. Catalogo de peliculas del SR ordenado");
		System.out.println("7. Recomendar peliculas por cada g�nero del SR");
		System.out.println("8. Lista de peliculas con tags asociados por g�nero");
		System.out.println("9. Recomendar peliculas seg�n las ultimas vistas");
		System.out.println("10. Lista ratings de una pelicula");
		System.out.println("11. Lista con los usuarios del SR m�s activos");
		System.out.println("12. Catalogo de usuarios del SR ordenado");
		System.out.println("13. Recomendar peliculas mayor n�mero de tags por g�nero del SR");
		System.out.println("14. Lista tags por g�nero");
		System.out.println("15. Recomendar usuarios seg�n las ultimas peliculas vistas");
		System.out.println("16. Lista tags de una pelicula");
		System.out.println("17. Revisar historial operaciones");
		System.out.println("18. Limpiar historial operaciones");
		System.out.println("19. Ver un n�mero de operaciones recientes");
		System.out.println("20. Eliminar un n�mero de operaciones recientes");
		System.out.println("21. Agregar pelicula");
		System.out.println("22. Agregar rating");
		System.out.println("23. Salir");
		System.out.println("Type the option number for the task, then press enter: (e.g., 1):");

	}
}
