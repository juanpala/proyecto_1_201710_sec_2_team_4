package api;


import model.data_structures.ILista;

public interface Istacks<T> {
	
	
	/**
	 * Crea una pila vac�a
	 */
	public 	void  Stack();
	/**
	 * Agrega un item al tope de la pila
	 */
	public void push(T item);
	/**
	 * Elimina el elemento en el tope de la pila
	 */
	public T pop();
	/**
	 * Indica si la pila est� vac�a
	 */
	public boolean isEmpty();
	/**
	 * N�mero de elementos en la pila

	 */
	public int size();
	

}
