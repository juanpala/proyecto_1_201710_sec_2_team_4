package api;

public interface IQueue<T> {

	
	public void Queue();
	
	public void enqueue(T item);
	
	public T dequeue();
	
	public boolean isEmpty();
}
